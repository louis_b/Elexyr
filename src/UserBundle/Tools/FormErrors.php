<?php

namespace src\UserBundle\Tools;

/**
 * Class FormErrors
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class FormErrors extends \app\Tools\FormErrors
{

    /** @var string FAILURE_AUTHENTICATION */
    const FAILURE_AUTHENTICATION = 'authentication';
    /** @var string FAILURE_PASSWORD_CONFIRMATION */
    const FAILURE_PASSWORD_CONFIRMATION = 'confirmation';

    /**
     * @override Add custom failures messages
     * @param string $failureType
     * @param string $fieldName
     * @param mixed $expectedValue
     *
     * @return bool|string
     */
    public function customFailureChecks($failureType, $fieldName, $expectedValue)
    {
        // authentication failure
        if ($fieldName === self::FAILURE_AUTHENTICATION && $expectedValue === true) {
            return $this->getAuthenticationFailureMessage();
        }
        // password confirmation failure
        if ($fieldName === self::FAILURE_PASSWORD_CONFIRMATION && $expectedValue === true) {
            return $this->getPasswordConfirmationFailureMessage();
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAuthenticationFailureMessage()
    {
        return "Cannot connect you with these information. Invalid email and password association";
    }

    /**
     * @return string
     */
    public function getPasswordConfirmationFailureMessage()
    {
        return "Password and Password confirmation must be identical";
    }
}
