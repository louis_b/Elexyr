<?php

namespace src\UserBundle\Tools;

use app\Facade\Tools;
use src\UserBundle\Repository\UserRepository;
use Repository;

/**
 * Class Email
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Email extends Tools
{

    /**
     * Check if email address already exists
     * @param string $email
     *
     * @return bool
     */
    public function isExistingAddress($email)
    {
        /** @var bool $isAvailable */
        $isAvailable = Repository::get('user')->isExistingValue(UserRepository::EMAIL_FIELD_CODE, $email);

        return $isAvailable;
    }
}
