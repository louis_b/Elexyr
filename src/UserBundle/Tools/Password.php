<?php

namespace src\UserBundle\Tools;

use app\Facade\Tools;

/**
 * Class Password
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Password extends Tools
{

    /**
     * Hash the submitted password
     * @param $password
     *
     * @return bool|string
     */
    public function getHashedPassword($password)
    {
        return password_hash($password,PASSWORD_DEFAULT);
    }

    /**
     * Check if a password correspond to a specific hash
     * @param string $password
     * @param string $hash
     *
     * @return bool
     */
    public function checkPasswordWithHash($password, $hash)
    {
        return password_verify($password, $hash);
    }
}
