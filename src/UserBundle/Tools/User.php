<?php

namespace src\UserBundle\Tools;

use Session;
use Tools;
use app\Orm\Entity;

/**
 * Class Page
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class User extends Tools
{

    /**
     * Return current user entity object
     * @return Entity
     */
    public function getCurrentUser()
    {
        return Session::_getData('current_user');
    }

    /**
     * Get current user Id
     * @return int
     */
    public function getCurrentUserId()
    {
        /** @var Entity $user */
        $user = Session::_getData('current_user');
        if (!$user instanceof Entity || !$user->getEntityId()) {
            return false;
        }

        return (int) $user->getEntityId();
    }

    /**
     * Get current user full name
     * @return string
     */
    public function getCurrentUserFullname()
    {
        /** @var Entity $user */
        $user = Session::_getData('current_user');

        return $user->getFirstname() . ' ' . $user->getLastname();
    }

    /**
     * Check if token is valid for a specific user
     * @param string $submittedToken
     * @param int $userId
     *
     * @return bool
     */
    public function isValidToken($submittedToken, $userId)
    {
        /** @var string $userToken */
        $userToken = Entity::get('User')->loadById($userId)->getConfirmationToken();
        if (empty($userToken)) {
            return false;
        }
        // test if token is valid
        if ($submittedToken == $userToken) {
            return true;
        }

        return false;
    }
}
