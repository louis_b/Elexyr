<?php

namespace src\UserBundle\Tools;

use app\Core\RouteManager;
use src\AppBundle\Tools\Emailing as ElexyrMailer;
use Entity;
use Tools;
use Repository;

/**
 * Class Emailing
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Emailing extends \app\Facade\Tools
{

    /** @var string RESET_PASSWORD_EMAIL_SUBJECT */
    const RESET_PASSWORD_EMAIL_SUBJECT = 'Your elexyr password reset request';

    /**
     * Set reset password email to specific user
     * @param string $userEmail
     */
    public function sendResetPasswordEmail($userEmail)
    {
        /** @var \src\AppBundle\Tools\Emailing $emailHelper */
        $emailHelper = Tools::get('app/emailing');
        /** @var \src\UserBundle\Repository\UserRepository $userRepository */
        $userRepository = Repository::get('User');
        /** @var int $userId */
        $userId = $userRepository->getIdFromEmail($userEmail);
        /** @var string $userName */
        $userName = $userRepository->getFullNameFromEmail($userEmail);
        /** @var string $resetToken */
        $resetToken = Tools::get('app/token')->generateUniqueToken();
        // add token in database
        /** @var \app\Orm\Entity $user */
        $user = Entity::get('User')->loadById($userId);
        $user->setConfirmationToken($resetToken)->save();

        // email content
        $content = $emailHelper->getTemplate('user/password_reset', [
            'header_image_src' => $emailHelper::EMAIL_HEADER_IMAGE_PATH,
            'reset_link_path' => RouteManager::getRoutePathFromCode('password_redefine', ['user_id' => $userId, 'reset_token' => $resetToken], 'user')
        ]);

        $emailHelper->send(
            ElexyrMailer::DEFAULT_ELEXYR_SENDER_ADDRESS_NOREPLY,
            ElexyrMailer::DEFAULT_ELEXYR_SENDER_NAME,
            [$userName => $userEmail],
            self::RESET_PASSWORD_EMAIL_SUBJECT,
            $content
        );
    }
}
