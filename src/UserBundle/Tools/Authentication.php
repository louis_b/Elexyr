<?php

namespace src\UserBundle\Tools;

use Session;
use Collection;
use Repository;
use Tools;
use OnyxDate;
use app\Core\Form;
use src\UserBundle\Repository\UserRepository;

/**
 * Class Password
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Authentication extends Tools
{

    /** @var string CURRENT_USER_SESSION_KEY */
    const CURRENT_USER_SESSION_KEY = 'current_user';

    /**
     * Register failures for login form related to authentication
     * @param Form $loginForm
     * @param string $emailAddress
     * @param string $enteredPassword
     *
     * @return Form
     */
    public function getManagedUserLoginForm(Form $loginForm, $emailAddress, $enteredPassword)
    {
        /** @var UserRepository $userRepository */
        $userRepository = Repository::get('user');
        /** @var bool|string $hashedPassword */
        $hashedPassword = false;
        if ($loginForm->isSubmit() === true) {
            /** @var string $hashedPassword */
            $hashedPassword = $userRepository->getHashedPasswordFromEmail($emailAddress);
        }
        // if no password is retrieve from database
        if ($hashedPassword === false || Tools::get('user/password')->checkPasswordWithHash($enteredPassword, $hashedPassword) !== true) {
            $loginForm->addFailure('authentication', 'invalid', true);

            return $loginForm;
        }

        return $loginForm;
    }

    /**
     * Save the user data in session
     * @param string $emailAddress
     *
     * @return bool
     */
    public function connectUser($emailAddress)
    {
        /** @var UserRepository $userRepository */
        $userRepository = Repository::get('user');
        /** @var int $userId */
        $userId = (int) $userRepository->getIdFromEmail($emailAddress);
        if ($userId === false) {
            return false;
        }

        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var Collection $currentUser */
        $currentUser = Collection::get('user')
            ->filterBy(UserRepository::ENTITY_ID_FIELD_CODE, $userId)
            ->filterBy('is_active', 1)
            ->filterBy('expires_at', $date, '>=')
            ->first();

        if ($currentUser instanceof \app\Orm\Entity && $currentUser->getEntityId()) {
            Session::_setData(self::CURRENT_USER_SESSION_KEY, $currentUser);

            return true;
        }

        // if expiration date is not valid check if it is empty
        $currentUser = Collection::get('user')
            ->filterBy(UserRepository::ENTITY_ID_FIELD_CODE, $userId)
            ->filterBy('is_active', 1)
            ->first();
        if ($currentUser instanceof \app\Orm\Entity && empty($currentUser->getExpiresAt())) {
            Session::_setData(self::CURRENT_USER_SESSION_KEY, $currentUser);

            return true;
        }

        return false;
    }

    /**
     * Unset current user data in session
     */
    public function disconnectCurrentUser()
    {
        Session::_unsetData(self::CURRENT_USER_SESSION_KEY);
    }
}
