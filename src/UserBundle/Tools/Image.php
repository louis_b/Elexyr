<?php

namespace src\UserBundle\Tools;

use app\Core\AssetManager;
use Tools;
use Collection;

/**
 * Class Image
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Image extends Tools
{

    /** @var string DEFAULT_IMAGE_USER_PATH */
    const DEFAULT_IMAGE_USER_PATH = 'global/user.png';
    /** @var string DEFAULT_IMAGE_BUNDLE */
    const DEFAULT_IMAGE_BUNDLE = 'app';

    /**
     * Get user main image object
     * @param int $userId
     *
     * @return \app\Orm\Entity
     */
    public function getUserMainImage($userId)
    {
        /** @var \app\Orm\Entity $image */
        $image = Collection::get('UserImage')
            ->filterBy('user_id', $userId)
            ->filterBy('is_main_image', 1)
            ->first();

        return $image;
    }

    /**
     * Get user main image path
     * @param int $userId
     *
     * @return string
     */
    public function getUserMainImagePath($userId)
    {
        /** @var \app\Orm\Entity $image */
        $image = $this->getUserMainImage($userId);
        if ($image instanceof \app\Orm\Entity && $image->getEntityId()) {
            return $image->getMediaPath('path');
        }

        // return default user image
        return AssetManager::getAssetFilePath(self::DEFAULT_IMAGE_USER_PATH, AssetManager::ASSET_TYPE_IMAGE, self::DEFAULT_IMAGE_BUNDLE);
    }
}
