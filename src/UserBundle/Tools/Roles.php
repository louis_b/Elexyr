<?php

namespace src\UserBundle\Tools;

use app\Core\BundleManager;
use app\Core\FileManager;
use app\Core\RouteManager;
use app\Orm\Entity;
use Tools;
use Parser;
use ErrorManager;

/**
 * Class Roles
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Roles extends Tools
{

    /** @var string NON_REGISTERED_USER_ROLE */
    const NON_REGISTERED_USER_ROLE = 'guest';
    /** @var string DEFAULT_ROUTE_REDIRECTION */
    const DEFAULT_ROUTE_REDIRECTION = 'user/login';
    /** @var string ROLES_CONFIGURATION_FILE */
    const ROLES_CONFIGURATION_FILE = 'Configs/roles.yml';

    /**
     * Get all roles configuration
     * @return array
     */
    public function getRolesConfiguration()
    {
        /** @var string $roleFilePath */
        $roleFilePath = BundleManager::getBundlePath('user');
        $roleFilePath .= str_replace(FileManager::CONFIG_DS, DS,self::ROLES_CONFIGURATION_FILE);
        if (!file_exists($roleFilePath)) {
            ErrorManager::log('Roles file configuration not find in ' . $roleFilePath);

            return [];
        }
        /** @var array $rolesConfiguration */
        $rolesConfiguration = Parser::get('yml')->getFileData($roleFilePath);

        return $rolesConfiguration;
    }

    /**
     * Get list of enable roles
     * @param bool $returnAllInformation
     *
     * @return array
     */
    public function getAvailableRoles($returnAllInformation = false)
    {
        /** @var array $rolesConfiguration */
        $rolesConfiguration = $this->getRolesConfiguration();
        /** @var array $availableRoles */
        $availableRoles = [];
        foreach ($rolesConfiguration as $roleName => $roleConfiguration) {
            if (isset($roleConfiguration['enable']) && $roleConfiguration['enable'] !== true) {
                continue;
            }
            if (isset($roleConfiguration['label']) && $returnAllInformation === false) {
                $availableRoles[$roleName] = $roleConfiguration['label'];
                continue;
            }
            if ($returnAllInformation === true) {
                $availableRoles[$roleName] = $roleConfiguration;
                continue;
            }
            $availableRoles[$roleName] = $roleName;
        }

        return $availableRoles;
    }

    /**
     * Get role label from code
     * @param string $roleCode
     *
     * @return string
     */
    public function getRoleLabel($roleCode)
    {
        /** @var array $roles */
        $roles = $this->getAvailableRoles();
        // if role label is defined
        if (isset($roles[$roleCode])) {
            return $roles[$roleCode];
        }

        return $roleCode;
    }

    /**
     * Get role data from configuration
     * @param string $roleCode
     *
     * @return array
     */
    public function getRoleData($roleCode)
    {
        /** @var array $roles */
        $roles = $this->getAvailableRoles(true);
        // if role code is defined
        if (isset($roles[$roleCode])) {
            return $roles[$roleCode];
        }

        return [];
    }

    /**
     * Return available role for a specific route
     * @param string $route
     * @param string $bundle
     *
     * @return array
     */
    public function getAllowedRolesForRoute($route, $bundle)
    {
        /** @var array $availableRoles */
        $availableRoles = $this->getAvailableRoles(true);
        $availableRoles = $this->getRouteAllowedRoleFromConfiguration($route, $availableRoles, $bundle);

        return $availableRoles;
    }

    /**
     * Manage route configuration to get roles that are allowed
     * @param string $route
     * @param array $availableRoles
     * @param string $bundle
     *
     * @return array
     */
    public function getRouteAllowedRoleFromConfiguration($route, array $availableRoles, $bundle)
    {
        /** @var array $routeConfiguration */
        $routeConfiguration = RouteManager::getRouteConfiguration($route, $bundle);

        // manage disallow
        /** @var array $disallowRole */
        $disallowRole = [];
        if (isset($routeConfiguration['disallow'])) {
            /** @var string $disallowRole */
            $disallowRole = $routeConfiguration['disallow'];
            /** @var array $disallowRole */
            $disallowRole = explode(',', $disallowRole);
        }
        /** @var array $availableRolesKeys */
        $availableRolesKeys = array_diff(array_keys($availableRoles), $disallowRole);
        $availableRolesKeys = array_values($availableRolesKeys);

        // manage allow
        /** @var int $allowedLimit */
        $allowedLimit = count($availableRolesKeys);
        if (isset($routeConfiguration['allow'])) {
            /** @var string $allowRole */
            $allowRole = $routeConfiguration['allow'];
            $allowedLimit = array_search($allowRole, $availableRolesKeys) + 1;
        }
        $availableRolesKeys = array_slice($availableRolesKeys, 0, $allowedLimit);

        // get only available roles data from all roles
        $availableRolesKeys = array_fill_keys($availableRolesKeys, $availableRolesKeys);
        $availableRoles = array_intersect_key($availableRoles, $availableRolesKeys);

        return $availableRoles;
    }

    /**
     * Check if current user is allowed for current route
     * @param string $route
     * @param string $bundle
     *
     * @return bool
     */
    public function isCurrentUserAllowedForRoute($route, $bundle)
    {
        /** @var string|bool $currentUserRole */
        $currentUserRole = $this->getCurrentUserRole();
        /** @var array $allowedRoles */
        $allowedRoles = $this->getAllowedRolesForRoute($route, $bundle);
        if (array_key_exists($currentUserRole, $allowedRoles)) {
            return true;
        }

        return false;
    }

    /**
     * Get current user role
     * @return string
     */
    public function getCurrentUserRole()
    {
        /** @var Entity $currentUser */
        $currentUser = Tools::get('user/user')->getCurrentUser();
        if ($currentUser instanceof Entity && $currentUser->getEntityId()) {
            return $currentUser->getRole();
        }

        return self::NON_REGISTERED_USER_ROLE;
    }

    /**
     * Get current user role redirection target
     * @return array
     */
    public function getRedirectionTarget()
    {
        /** @var string $role */
        $role = $this->getCurrentUserRole();
        /** @var array $role */
        $role = $this->getRoleData($role);
        /** @var string $target */
        $target = self::DEFAULT_ROUTE_REDIRECTION;
        if (isset($role['redirect'])) {
            $target = $role['redirect'];
        }

        return explode('/', $target);
    }
}
