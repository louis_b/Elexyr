<?php

namespace src\UserBundle\Controller;

use Repository;
use Entity;
use app\Core\RouteManager;

/**
 * Class UserImageController
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class UserImageController
{

    /**
     * Set user image as main image
     * @param array $params
     */
    public function setMainImageAction(array $params)
    {
        // if a param is missing
        if (!isset($params['user_id']) || !isset($params['image_id'])) {
            RouteManager::redirectToRoute('user_list');
        }
        /** @var int $userId */
        $userId = (int) $params['user_id'];
        /** @var int $imageId */
        $imageId = (int) $params['image_id'];
        // set main image
        Repository::get('UserImage')->setMainImage($userId, $imageId);

        // redirect to user page
        RouteManager::redirectToRoute('user_edit', ['user_id' => $userId]);
    }

    /**
     * Delete a specific image
     * @param array $params
     */
    public function deleteAction(array $params)
    {
        if (!isset($params['image_id']) || !$params['user_id']) {
            RouteManager::redirectToRoute('user_list');
        }
        /** @var int $userId */
        $userId = (int) $params['user_id'];
        /** @var int $imageId */
        $imageId = (int) $params['image_id'];

        /** @var \app\Orm\Entity $imageToDelete */
        $imageToDelete = Entity::get('UserImage')->loadById($imageId);
        $imageToDelete->delete();

        // redirect to user list page
        RouteManager::redirectToRoute('user_edit', ['user_id' => $userId]);
    }
}
