<?php

namespace src\UserBundle\Controller\Error;

use Request;
use Form;
use Template;

/**
 * Class Error404Controller
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Error404Controller
{

    /**
     * 404 page controller action
     */
    public function indexAction()
    {
        Template::get('user/page404');
    }
}
