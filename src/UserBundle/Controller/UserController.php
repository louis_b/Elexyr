<?php

namespace src\UserBundle\Controller;

use app\Core\FormManager;
use app\Core\RouteManager;
use app\Orm\Query\Select;
use Repository;
use Template;
use Request;
use Form;
use OnyxDate;
use Tools;
use Collection;
use Entity;

/**
 * Class UserController
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class UserController
{

    /**
     * Create user action
     * @param array $params
     */
    public function createAction(array $params)
    {
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $createForm */
        $createForm = Form::get('user/userCreate');
        if ($createForm->isSubmit() === true && isset($params['user_password'])) {
            /** @var string $password */
            $password = Tools::get('user/password')->getHashedPassword($params['user_password']);
            // manage checkbox value
            $createForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($createForm, ['is_active'], 0);

            $createForm->setFieldValue('password', $password);
            $createForm->setFieldValue('created_at', $date);
            $createForm->setFieldValue('updated_at', $date);
        }
        $createForm->submit();

        // manage user image creation
        if ($createForm->getIsValid() === true && FormManager::isEmptyFile($params['user_image']) === false) {
            /** @var Entiy $createdUser */
            $createdUser = Collection::get('User')->getLastEntity();
            // create new image
            Entity::get('UserImage')
                ->setUserId($createdUser->getEntityId())
                ->setPath($createForm->getFieldValue('user_image'))
                ->setIsMainImage(1)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->save();
        }
        if ($createForm->getIsValid() === true) {
            RouteManager::redirectToRoute('user_list');
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($createForm);

        /** @var array $rolesValues */
        $rolesValues = Tools::get('user/roles')->getAvailableRoles();
        /** @var array $viewParams */
        $viewParams = [
            'createForm' => $createForm,
            'formErrors' => $formErrors,
            'roles' => $rolesValues,
            'formActionParams' => [],
            'submitMessage' => 'Create User'
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit user action
     * @param $params
     */
    public function editAction(array $params)
    {
        // if param is not set redirect to creation form
        if (!isset($params['user_id'])) {
            RouteManager::redirectToRoute('user_create');
        }
        /** @var int $userId */
        $userId = (int) $params['user_id'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $editForm */
        $editForm = Form::get('user/userEdit')->setEntityId($userId);
        if ($editForm->isSubmit() === true && isset($params['user_password']) && !empty($params['user_password'])) {
            /** @var string $password */
            $password = Tools::get('user/password')->getHashedPassword($params['user_password']);
            $editForm->setFieldValue('password', $password);
        }
        if ($editForm->isSubmit() === true) {
            // manage checkbox value
            $editForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($editForm, ['is_active'], 0);

            $editForm->setFieldValue('updated_at', $date);
        }
        $editForm->submit();

        // manage user image creation
        if ($editForm->getIsValid() === true && FormManager::isEmptyFile($params['user_image']) === false) {
            // reset main image
            Repository::get('UserImage')->resetMainImage($userId);
            // create new image
            Entity::get('UserImage')
                ->setUserId($userId)
                ->setPath($editForm->getFieldValue('user_image'))
                ->setIsMainImage(1)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->save();
        }
        if ($editForm->getIsValid() === true) {
            RouteManager::redirectToRoute('user_edit', ['user_id' => $userId]);
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($editForm);

        /** @var array $userImages */
        $userImages = Collection::get('UserImage')
            ->filterBy('user_id', $userId)
            ->sortBy('is_main_image', Select::SORT_ORDER_DESC)
            ->all();

        /** @var array $rolesValues */
        $rolesValues = Tools::get('user/roles')->getAvailableRoles();
        /** @var array $viewParams */
        $viewParams = [
            'createForm' => $editForm,
            'formErrors' => $formErrors,
            'roles' => $rolesValues,
            'formActionParams' => ['user_id' => $userId],
            'submitMessage' => 'Update User',
            'userImages' => $userImages,
            'userId' => $userId
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Delete specific user
     * @param array $params
     */
    public function deleteAction(array $params)
    {
        // current user cannot delete himself
        /** @var int $currentUserId */
        $currentUserId = Tools::get('user/user')->getCurrentUserId();
        if (!isset($params['user_id']) || $currentUserId == $params['user_id']) {
            RouteManager::redirectToRoute('user_list');
        }

        /** @var \app\Orm\Entity $userToDelete */
        $userToDelete = Entity::get('User')->loadById($params['user_id']);
        $userToDelete->delete();

        // redirect to user list page
        RouteManager::redirectToRoute('user_list');
    }

    /**
     * User listing action
     * @param array $params
     */
    public function listAction(array $params)
    {
        /** @var int $pageNumber */
        $pageNumber = 1;
        if (isset($params['page_number']) && $params['page_number'] > 1) {
            $pageNumber = (int) $params['page_number'];
        }

        /** @var \src\UserBundle\Tools\Page $pageHelper */
        $pageHelper = Tools::get('user/page');
        /** @var \app\Orm\Collection $usersCollection */
        $usersCollection = Collection::get('user');
        $usersCollection = $pageHelper->addLimitToCollection($usersCollection, $pageNumber);
        $usersCollection = $usersCollection->all();
        // if collection is empty redirect to first page
        if (count($usersCollection) <= 0 && $pageNumber != 1) {
            RouteManager::redirectToRoute('user_list');
        }

        /** @var int|bool $previousPage */
        $previousPage = $pageHelper->getPreviousPage($pageNumber);
        /** @var int|bool $nextPage */
        $nextPage = $pageHelper->getNextPage($pageNumber);
        /** \app\Orm\Entity */
        $currentUser = Tools::get('user/user')->getCurrentUser();
        /** @var array $viewParams */
        $viewParams = [
            'page' => $pageNumber,
            'usersCollection' => $usersCollection,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'rolesHelper' => Tools::get('user/roles'),
            'currentUser' => $currentUser,
            'currentUserId' => $currentUser->getEntityId(),
            'currentUserRole' => $currentUser->getRole(),
            'userImageHelper' => Tools::get('user/image')
        ];
        Template::get()->setParams($viewParams);
    }
}
