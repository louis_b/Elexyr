<?php

namespace src\UserBundle\Controller;

use app\Core\RouteManager;
use src\UserBundle\Tools\Authentication;
use Form;
use Tools;
use Template;
use Entity;

/**
 * Class UserController
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class AuthenticationController
{

    /**
     * Login Action
     * @param array $params
     */
    public function loginAction(array $params)
    {
        /** @var Authentication $authenticationHelper */
        $authenticationHelper = Tools::get('user/authentication');
        /** @var bool $authenticationSuccess */
        $authenticationSuccess = false;
        /** @var \app\Core\Form $loginForm */
        $loginForm = Form::get('user/login');
        // manage authentication
        if (isset($params['email']) && isset($params['password'])) {
            $loginForm = $authenticationHelper->getManagedUserLoginForm($loginForm, $params['email'], $params['password']);
        }
        // submit form
        $loginForm->submit();
        // on authentication success
        if ($loginForm->getIsValid() === true) {
            $authenticationSuccess = $authenticationHelper->connectUser($params['email']);
        }
        // redirect on authentication success
        if ($authenticationSuccess === true) {
            RouteManager::redirectToRoute('home', [], 'app');
        }

        // manage form failures
        $formErrors = Tools::get('user/formErrors')->getFormattedFormErrors($loginForm);
        /** @var array $viewParams */
        $viewParams = [
            'loginForm' => $loginForm,
            'formErrors' => $formErrors,
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Logout current user
     * @param array $params
     */
    public function logoutAction(array $params)
    {
        Tools::get('user/authentication')->disconnectCurrentUser();

        RouteManager::redirectToRoute('login');
    }

    /**
     * Reset password form action
     * @param array $params
     */
    public function resetPasswordAction(array $params)
    {
        /** @var \app\Core\Form $resetForm */
        $resetForm = Form::get('user/passwordReset');
        // submit form
        $resetForm->submit();

        /** @var bool $isExistingEmail */
        $isExistingEmail = false;
        // on success
        if ($resetForm->getIsValid() === true) {
            /** @var string $userEmail */
            $userEmail = $resetForm->getFieldValue('email');
            $isExistingEmail = Tools::get('user/email')->isExistingAddress($userEmail);
        }
        // send reset password request
        /** @var array $formSuccess */
        $formSuccess = [];
        if ($resetForm->getIsValid() === true && $isExistingEmail === true) {
            Tools::get('user/emailing')->sendResetPasswordEmail($userEmail);
            $formSuccess[] = 'Reset password email has been sent successfully';
        }

        // manage form failures
        $formErrors = Tools::get('user/formErrors')->getFormattedFormErrors($resetForm);
        if ($resetForm->getIsValid() === true && $isExistingEmail === false) {
            $formErrors[] = 'Unknown email address, are you sure to be registered on this Elexyr website ?';
        }

        /** @var array $viewParams */
        $viewParams = [
            'resetForm' => $resetForm,
            'formErrors' => $formErrors,
            'formSuccess' => $formSuccess,
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Define a new password
     * @param array $params
     */
    public function redefinePasswordAction(array $params)
    {
        if (!isset($params['user_id']) || !isset($params['reset_token'])) {
            RouteManager::redirectToRoute('login');
        }
        /** @var int $userId */
        $userId = $params['user_id'];
        /** @var bool $isValidToken */
        $isValidToken = Tools::get('user/user')->isValidToken($params['reset_token'], $userId);
        if ($isValidToken !== true) {
            RouteManager::redirectToRoute('login');
        }

        /** @var \app\Core\Form $resetForm */
        $redefineForm = Form::get('user/passwordRedefine')->setEntityId($userId);
        // register failure if password confirmation is not valid
        if (isset($params['user_password']) && isset($params['password_confirmation']) && $params['user_password'] != $params['password_confirmation']) {
            $redefineForm->addFailure('confirmation', 'invalid', true);
        }
        // hash password before saving it in database
        if ($redefineForm->isSubmit() === true && isset($params['user_password']) && !empty($params['user_password'])) {
            /** @var string $password */
            $password = Tools::get('user/password')->getHashedPassword($params['user_password']);
            $redefineForm->setFieldValue('password', $password);
        }
        // submit form
        $redefineForm->submit();

        if ($redefineForm->getIsValid() === true) {
            /** @var \app\Orm\Entity $user */
            $user = Entity::get('User')->loadById($userId);
            /** @var string $emailAddress */
            $emailAddress = $user->getEmail();
            // flush user token after use
            $user->setConfirmationToken(null)->save();

            // connect user after password reset
            Tools::get('user/authentication')->connectUser($emailAddress);
            RouteManager::redirectToRoute('home', [], 'app');
        }

        // manage form failures
        $formErrors = Tools::get('user/formErrors')->getFormattedFormErrors($redefineForm);

        /** @var array $viewParams */
        $viewParams = [
            'userId' => $userId,
            'userToken' => $params['reset_token'],
            'redefineForm' => $redefineForm,
            'formErrors' => $formErrors,
        ];
        Template::get()->setParams($viewParams);
    }
}