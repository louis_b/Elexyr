<?php

namespace src\UserBundle\Repository;

use app\Orm\Repository;

/**
 * Class UserImageRepository
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class UserImageRepository extends Repository
{

    /**
     * Set existing image to non main user image
     * @param int $userId
     *
     * @return Repository
     */
    public function resetMainImage($userId)
    {
        $this->update()->set(["is_main_image" => 0])->where([['user_id', '=', $userId]]);

        return $this;
    }

    /**
     * Set main user image
     * @param int $userId
     * @param int $imageId
     *
     * @return Repository
     */
    public function setMainImage($userId, $imageId)
    {
        $this->resetMainImage($userId);
        $this->update()->set(["is_main_image" => 1])->where([
            ['entity_id', '=', $imageId],
            ['user_id', '=', $userId]
        ]);

        return $this;
    }
}
