<?php

namespace src\UserBundle\Repository;

use app\Orm\Repository;

/**
 * Class UserRepository
 *
 * @package Onyx\UserBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class UserRepository extends Repository
{

    /** @var string ENTITY_ID_FIELD_CODE */
    const ENTITY_ID_FIELD_CODE = 'entity_id';
    /** @var string EMAIL_FIELD_CODE */
    const EMAIL_FIELD_CODE = 'email';
    /** @var string PASSWORD_FIELD_CODE */
    const PASSWORD_FIELD_CODE = 'password';
    /** @var string FIRSTNAME_FIELD_CODE */
    const FIRSTNAME_FIELD_CODE = 'firstname';
    /** @var string LASTNAME_FIELD_CODE */
    const LASTNAME_FIELD_CODE = 'lastname';

    /**
     * Get hashed password corresponding to a specific email address
     * @param string $email
     *
     * @return string|false
     */
    public function getHashedPasswordFromEmail($email)
    {
        /** @var string $hashedPassword */
        $hashedPassword = $this->select()->columns([self::PASSWORD_FIELD_CODE])->where([[self::EMAIL_FIELD_CODE, '=', $email]]);
        if (isset($hashedPassword[0][self::PASSWORD_FIELD_CODE])) {
            return $hashedPassword[0][self::PASSWORD_FIELD_CODE];
        }

        return false;
    }

    /**
     * Get user id corresponding to a specific email address
     * @param string $email
     *
     * @return int|false
     */
    public function getIdFromEmail($email)
    {
        /** @var string $hashedPassword */
        $hashedPassword = $this->select()->columns([self::ENTITY_ID_FIELD_CODE])->where([[self::EMAIL_FIELD_CODE, '=', $email]]);
        if (isset($hashedPassword[0][self::ENTITY_ID_FIELD_CODE])) {
            return $hashedPassword[0][self::ENTITY_ID_FIELD_CODE];
        }

        return false;
    }

    /**
     * Get user first name and last name from email address
     * @param string $email
     *
     * @return string
     */
    public function getFullNameFromEmail($email)
    {
        /** @var array $result */
        $result = $this->select()->columns([self::FIRSTNAME_FIELD_CODE, self::LASTNAME_FIELD_CODE])->where([[self::EMAIL_FIELD_CODE, '=', $email]]);
        if (isset($result[0][self::FIRSTNAME_FIELD_CODE]) && isset($result[0][self::LASTNAME_FIELD_CODE])) {
            return $result[0][self::FIRSTNAME_FIELD_CODE] . ' ' . $result[0][self::LASTNAME_FIELD_CODE];
        }

        return '';
    }
}
