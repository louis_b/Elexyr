<?php

namespace src\PluginBundle\Controller;

use Request;
use Template;
use Tools;

/**
 * Class PluginController
 *
 * @package Elexyr\PluginBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class PluginController
{

    /**
     * List all plugins action
     * @param array $params
     */
    public function listAction(array $params)
    {
        /** @var array $allPlugins */
        $allPlugins = Tools::get('plugin/data')->getAllPlugins();

        /** @var array $viewParams */
        $viewParams = [
            'numberPlugin' => count($allPlugins),
            'allPlugins' => $allPlugins,
        ];
        Template::get()->setParams($viewParams);
    }
}
