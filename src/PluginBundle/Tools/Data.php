<?php

namespace src\PluginBundle\Tools;

use app\Core\BundleManager;
use app\Core\FileManager;
use Tools;
use Parser;
use ErrorManager;

/**
 * Class Data
 *
 * @package Elexyr\PluginBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Data extends Tools
{

    /** @var string PLUGINS_CONFIGURATION_FILE */
    const PLUGINS_CONFIGURATION_FILE = 'Configs/plugins.yml';

    /**
     * Get all plugins from configuration file
     * @return array
     */
    public function getAllPlugins()
    {
        /** @var string $pluginsFilePath */
        $pluginsFilePath = BundleManager::getBundlePath('plugin');
        $pluginsFilePath .= str_replace(FileManager::CONFIG_DS, DS,self::PLUGINS_CONFIGURATION_FILE);

        if (!file_exists($pluginsFilePath)) {
            ErrorManager::log('Plugins configuration file not find in ' . $pluginsFilePath);

            return [];
        }
        /** @var array $pluginsData */
        $pluginsData = Parser::get('yml')->getFileData($pluginsFilePath);

        return $pluginsData;
    }

    /**
     * Get number of installed plugins
     * @return int
     */
    public function getPluginsCount()
    {
        /** @var array $allPlugins */
        $allPlugins = $this->getAllPlugins();

        return count($allPlugins);
    }
}
