<?php

namespace src\ThemeBundle\Controller;

use Request;
use Template;

/**
 * Class ThemesController
 *
 * @package Elexyr\ThemesBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class ThemesController
{

    /**
     * Display themes page
     * @param array $params
     */
    public function themesAction(array $params)
    {
        Template::get();
    }

}
