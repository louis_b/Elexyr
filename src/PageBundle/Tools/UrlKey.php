<?php

namespace src\PageBundle\Tools;

/**
 * Class UrlKey
 *
 * @package Elexyr\PageBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class UrlKey extends \app\Facade\Tools
{

    /**
     * Format string to be a valid url_key
     * @param string $urlKey
     *
     * @return string
     */
    public function generateValidUrlKey($urlKey)
    {
        // replace spaces
        /** @var string $urlKey */
        $urlKey = str_replace(' ', '-', $urlKey);
        $urlKey = strtolower($urlKey);

        return $urlKey;
    }
}
