<?php

namespace src\PageBundle\Tools;

use app\Facade\Tools;
use Repository;
use Entity;
use Collection;
use app\Orm\Query\Select;

class Data extends Tools
{

    /** @var string HOMEPAGE_URI */
    const HOMEPAGE_URI = '/';
    /** @var string PAGE_MASS_ACTION_PREFIX */
    const PAGE_MASS_ACTION_PREFIX = 'page-mass';

    /**
     * @param $pageId
     * @param null $allData
     * @return array
     */
    public function getPageData($pageId, $allData = null){
        /** @var array $pageDatas */
        $pageDatas = [];

        /** @var \app\Orm\Collection $sectionCollections */
        $sectionCollections = Collection::get('Section')
            ->filterBy('page_id', $pageId, '=')
            ->sortBy('position', Select::SORT_ORDER_ASC)
            ->all();

        // get all page data with ContentType
        if ($allData === true){
            foreach ($sectionCollections as $key => $sectionCollection){
                $pageDatas['section' . $key]['section'] = $sectionCollection;
                $pageDatas['section' . $key]['contents'] = [];

                $contentOnly['section' . $key]['allContents'] = Collection::get('Content')
                    ->filterBy('section_id', $sectionCollection->getEntityId(), '=')
                    ->sortBy('position', Select::SORT_ORDER_ASC)
                    ->all();

                for ($i = 0; $i < $sectionCollection->getNbDivs(); $i++){
                    $pageDatas['section' . $key]['contents']['content' . $i] = [];
                    $pageDatas['section' . $key]['contents']['content' . $i]['data'] = $contentOnly['section' . $key]['allContents'][$i];
                    if (!empty($pageDatas['section' . $key]['contents']['content' . $i]['data']->getContentType())){
                        $currentContent = $pageDatas['section' . $key]['contents']['content' . $i]['data'];
                        $pageDatas['section' . $key]['contents']['content' . $i]['contentType'] = Collection::get($currentContent->getContentType())
                            ->filterBy('entity_id', $currentContent->getContentTypeId(), '=')
                            ->all();
                    }
                }
            }

            return $pageDatas;

        }

        // get data without ContentType
        foreach ($sectionCollections as $key => $sectionCollection){
            $pageDatas['section' . $key]['section'] = $sectionCollection;

            $pageDatas['section' . $key]['contents'] = Collection::get('Content')
                ->filterBy('section_id', $sectionCollection->getEntityId(), '=')
                ->sortBy('position', Select::SORT_ORDER_ASC)
                ->all();
        }

        return $pageDatas;
    }

    /**
     * Check if specific page is Homepage
     * @param \app\Orm\Entity $page
     *
     * @return bool
     */
    public function isHomePage(\app\Orm\Entity $page)
    {
        if ($page->getUrlKey() === self::HOMEPAGE_URI) {
            return true;
        }

        return false;
    }
}
