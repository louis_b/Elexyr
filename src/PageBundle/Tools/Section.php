<?php

namespace src\PageBundle\Tools;

use app\Facade\Tools;
use Repository;

class Section extends Tools
{
    public function contentItemsToJSON($sectionId)
    {
        $sectionContentIds = Repository::get('Content')->getSectionContentIds($sectionId);
        return json_encode($sectionContentIds);
    }
}