<?php

namespace src\PageBundle\Tools;

use app\Core\BundleManager;
use app\Core\FileManager;
use app\Core\RouteManager;
use app\Orm\Entity;
use Tools;
use Parser;
use ErrorManager;

class ContentType extends Tools
{
    const CONTENT_TYPES_CONFIGURATION_FILE = 'Configs/contentTypes.yml';

    /**
     * Get all contentTypes configuration
     * @return array
     */
    public function getAllContentTypes()
    {
        /** @var string $contentTypesFilePath */
        $contentTypesFilePath = BundleManager::getBundlePath('page');
        $contentTypesFilePath .= str_replace(FileManager::CONFIG_DS, DS,self::CONTENT_TYPES_CONFIGURATION_FILE);

        if (!file_exists($contentTypesFilePath)) {
            ErrorManager::log('Content types configuration file not find in ' . $contentTypesFilePath);

            return [];
        }
        /** @var array $allContentTypes */
        $allContentTypes = Parser::get('yml')->getFileData($contentTypesFilePath);
        /** @var array $pluginContentTypes */
        $pluginContentTypes = Tools::get('plugin/data')->getAllPlugins();
        // merge available content types and plugins
        $allContentTypes = array_merge($allContentTypes, $pluginContentTypes);

        return $allContentTypes;
    }

    /**
     * Get all contentTypes header
     * @return array
     */
    public function getAllContentTypesHeader()
    {
        /** @var array $allContentTypes */
        $allContentTypes = $this->getAllContentTypes();
        /** @var array $headerContentTypes */
        $headerContentTypes = [];
        foreach ($allContentTypes as $contentType) {
            if (isset($contentType['is_header']) && $contentType['is_header'] === true) {
                $headerContentTypes[] = $contentType;
            }
        }

        return $headerContentTypes;
    }
}