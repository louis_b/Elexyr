<?php

namespace src\PageBundle\Controller;

use app\Core\RouteManager;
use app\Orm\Query\Select;
use Request;
use Template;
use Form;
use Tools;
use OnyxDate;
use Collection;
use Entity;
use Repository;

/**
 * Class PagesController
 *
 * @package Elexyr\PageBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class PagesController
{

    /**
     * Display pages page
     * @param array $params
     */
    public function pagesAction(array $params)
    {
        /** @var \app\Core\Form $form */
        $form = Form::get('page/pageCreate');
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var int $pageNumber */
        $pageNumber = 1;
        if (isset($params['page_number']) && $params['page_number'] > 1) {
            $pageNumber = (int) $params['page_number'];
        }

        if ($form->isSubmit() === true && isset($params['name'])) {
            /** @var string $defaultUrlKey */
            $defaultUrlKey = Repository::get('Page')->getDefaultUrlKey($params['name']);

            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
            $form->setFieldValue('url_key', $defaultUrlKey);
        }

        /** @var \src\UserBundle\Tools\Page $pageHelper */
        $pageHelper = Tools::get('page/page');
        /** @var \app\Orm\Collection $pageCollection */
        $pageCollection = Collection::get('Page');
        $pageCollection = $pageHelper->addLimitToCollection($pageCollection, $pageNumber);
        $pageCollection = $pageCollection->sortBy('updated_at', Select::SORT_ORDER_DESC)->all();
        // if collection is empty redirect to first page
        if (count($pageCollection) <= 0 && $pageNumber != 1) {
            RouteManager::redirectToRoute('page_list');
        }

        // submit form
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var int|bool $previousPage */
        $previousPage = $pageHelper->getPreviousPage($pageNumber);
        /** @var int|bool $nextPage */
        $nextPage = $pageHelper->getNextPage($pageNumber);

        /** @var array $viewParams */
        $viewParams = [
            'creationForm' => $form,
            'pageCollection' => $pageCollection,
            'formErrors' => $formErrors,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'pageHelper' => Tools::get('page/data')
        ];
        // render view
        Template::get()->setParams($viewParams);
    }

    /**
     * Define homepage
     * @param array $params
     */
    public function homepageAction(array $params)
    {
        if (!isset($params['page_id'])) {
            RouteManager::redirectToRoute('page_list');
        }
        /** @var int $pageId */
        $pageId = $params['page_id'];
        /** @var \src\PageBundle\Tools\Data $pageHelper */
        $pageHelper = Tools::get('page/data');
        /** @var string $homepageUrlKey */
        $homepageUrlKey = $pageHelper::HOMEPAGE_URI;
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        // manage current homepage url key
        /** @var \app\Orm\Entity $currentHomepage */
        $currentHomepage = Entity::get('Page')->loadBy('url_key', $homepageUrlKey);
        if ($currentHomepage instanceof \app\Orm\Entity && $currentHomepage->getEntityId()) {
            /** @var string $currentHomepageName */
            $currentHomepageName = $currentHomepage->getName();
            /** @var string $newurlkey */
            $newUrlkey = Repository::get('Page')->getDefaultUrlKey($currentHomepageName);
            $currentHomepage->setUrlKey($newUrlkey)
                ->setUpdatedAt($date)
                ->save();
        }

        // set new homepage url key
        Entity::get('Page')->loadById($pageId)->setUrlKey($homepageUrlKey)->save();

        RouteManager::redirectToRoute('page_list');
    }

    /**
     * enable page on click
     * @param array $params
     */
    public function pageEnableAction(array $params)
    {
        /** @var int $pageId */
        $pageId = $params['id'];
        /** @var string $date */
        $date = OnyxDate::get()->toString();
        /** Entity */
        Entity::get('Page')->loadById($pageId)
            ->setIsActive(1)
            ->setUpdatedAt($date)
            ->save();

        RouteManager::redirectToRoute('page_list');
    }

    /**
     * disable page on click
     * @param array $params
     */
    public function pageDisableAction(array $params)
    {
        /** @var int $pageId */
        $pageId = $params['id'];
        /** @var string $date */
        $date = OnyxDate::get()->toString();
        /** Entity */
        Entity::get('Page')->loadById($pageId)
            ->setIsActive(0)
            ->setUpdatedAt($date)
            ->save();

        RouteManager::redirectToRoute('page_list');
    }

    /**
     * Delete specific page
     * @param array $params
     */
    public function pageDeleteAction(array $params)
    {
        if (!isset($params['id'])){
            RouteManager::redirectToRoute('page');
        }

        /** @var int $navigationId */
        $pageId = $params['id'];

        Entity::get('Page')->loadById($pageId)->delete();
        Repository::get('Section')->deleteAllSection($pageId);
        Repository::get('Content')->deleteAllContent($pageId);

        // redirect to pages page
        RouteManager::redirectToRoute('page_list');
    }

    /**
     * Page builder action
     * @param array $params
     */
    public function customPageAction(array $params)
    {
        if (!isset($params['id'])) {
            RouteManager::redirectToRoute('page_list');
        }
        /** @var int $currentPageId */
        $currentPageId = $params['id'];

        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Orm\Entity $currentePageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);
        $currentSectionsNumber = $currentPageItem->getSectionsNumber();

        /**
         * create section and content for the first time
         */
        /** @var \app\Core\Form $itemUpdateForm */
        $itemUpdateForm = Form::get('page/pageSection')->setEntityObject($currentPageItem);
        if ($itemUpdateForm->isSubmit() === true) {
            $itemUpdateForm->setFieldValue('updated_at', $date);
            /** @var int $sectionToSave */
            $sectionToSave = (int) $params['sections_number'];

            /**
             * create section
             */
            for ($i = 0; $i < $sectionToSave; $i++){
                Entity::get('Section')->setPageId($currentPageId)
                    ->setPosition($i)
                    ->setCreatedAt($date)
                    ->setUpdatedAt($date)
                    ->save();
            }
            /**
             * create content
             */
            $currentSections = Collection::get('Section')
                ->filterBy('page_id', $currentPageId, '=')
                ->all();

            foreach ($currentSections as $currentSection){
                Entity::get('Content')->setPageId($currentPageId)
                    ->setSectionId($currentSection->getEntityId())
                    ->setPosition(0)
                    ->setCreatedAt($date)
                    ->setUpdatedAt($date)
                    ->save();
            }
        }

        /** @var array $sectionCollections */
        $pageData = [];
        if ($currentSectionsNumber > 0){
            $pageData = Tools::get('page/data')->getPageData($currentPageId);
        }
        // submit form
        $itemUpdateForm->submit();

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'currentPageId' => $currentPageId,
            'currentPageItem' => $currentPageItem,
            'creationForm' => $itemUpdateForm,
            'currentSectionsNumber' => $currentSectionsNumber,
            'sectionCollections' => $pageData,
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit page information action
     * @param array $params
     */
    public function editPageAction(array $params)
    {
        if (!isset($params['page_id'])) {
            RouteManager::redirectToRoute('page_list');
        }
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var \app\Orm\Entity $currentPage */
        $currentPage = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Core\Form $updateForm */
        $updateForm = Form::get('page/pageEdit')->setEntityObject($currentPage);

        if ($updateForm->isSubmit() === true && isset($params['url_key'])) {
            /** @var string $urlKey */
            $urlKey = Tools::get('page/urlKey')->generateValidUrlKey($params['url_key']);
            $updateForm->setFieldValue('url_key', $urlKey);
        }
        if ($updateForm->isSubmit() === true) {
            $updateForm->setFieldValue('updated_at', $date);
            $updateForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($updateForm, ['is_active'], 0);
        }
        // submit form
        $updateForm->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($updateForm);

        /** @var array $viewParams */
        $viewParams = [
            'currentPageId' => $currentPageId,
            'currentPage' => $currentPage,
            'updateForm' => $updateForm,
            'formErrors' => $formErrors,
            'currentPageName' => $currentPage->getName(),
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($currentPage->getUrlKey())
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Mass delete for pages
     * @param array $params
     */
    public function massDeleteAction(array $params)
    {
        /** @var array $pageIdsToDelete */
        $pageIdsToDelete = [];

        /** @var \src\PageBundle\Tools\Data $pageHelper */
        $pageHelper = Tools::get('page/data');
        /** @var string $pageParamPrefix */
        $pageParamPrefix = $pageHelper::PAGE_MASS_ACTION_PREFIX;
        // format ids to delete by entities
        foreach ($params as $key => $param) {
            if (strpos($key, $pageParamPrefix) !== false) {
                $pageIdsToDelete[] = str_replace($pageParamPrefix . '-', '', $key);
            }
        }

        // mass delete in database
        Repository::get('Page')->massDelete($pageIdsToDelete);

        // redirect to page home
        RouteManager::redirectToRoute('page_list');
    }
}
