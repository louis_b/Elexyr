<?php

namespace src\PageBundle\Controller;

use Template;
use Entity;
use Tools;
use OnyxDate;
use Form;
use Collection;
use app\Core\RouteManager;

/**
 * Class ContentTextController
 *
 * @package Elexyr\PageBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class ContentTextController
{

    /** @var string CONTENT_TEXT_TYPE */
    const CONTENT_TEXT_TYPE = 'ContentText';

    /**
     * Create content text
     * @param array $params
     */
    public function addAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['id'];

        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\Form $form */
        $form = Form::get('page/ContentTextCreate');
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        /** create form */
        if ($form->isSubmit() && isset($params['font_size'])) {
            // manage checkbox value
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['display_title'], 0);
            /** @var int $fontSize */
            $fontSize = (int) $params['font_size'];

            $form->setFieldValue('content_id', $currentContentId);
            $form->setFieldValue('font_size', $fontSize);
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();

        /* ------------------
           DELETE EXISTING CONTENT TYPE ASSOCIATED TO CURRENT CONTENT
        */
        /** @var \app\Orm\Entity $currentContent */
        $currentContent = Entity::get('Content')->loadById($currentContentId);
        /** @var null|\app\Orm\Entity $existingContentType */
        $existingContentType = null;
        if ($form->getIsValid() === true && !empty($currentContent->getContentTypeId()) && !empty($currentContent->getContentType())) {
            $existingContentType = Entity::get($currentContent->getContentType())->loadById($currentContent->getContentTypeId());
        }
        if ($existingContentType instanceof \app\Orm\Entity && $existingContentType->getEntityId()) {
            $existingContentType->delete();
        }

        /* ------------------
           MANAGE CONTENT DATA IS GLOBAL CONTENT OBJECT
        */
        if ($form->getIsValid() === true) {
            /** @var \app\Orm\Entity $createdContentText */
            $createdContentText = Collection::get(self::CONTENT_TEXT_TYPE)->getLastEntity();

            $currentContent->setContentTypeId($createdContentText->getEntityId());
            $currentContent->setContentType(self::CONTENT_TEXT_TYPE);
            $currentContent->setUpdatedAt($date);
            $currentContent->save();

            // add page update date
            $currentPageItem->setUpdatedAt($date)->save();

            // redirect on success
            RouteManager::redirectToRoute('content_add', ['page_id' => $currentPageId, 'id' => $currentContentId]);
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'currentPageItem' => $currentPageItem,
            'currentContentId' => $currentContentId,
            'formErrors' => $formErrors,
            'formParams' => [
                'page_id' => $currentPageItem->getEntityId(),
                'id' => $currentContentId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit content text action
     * @param array $params
     */
    public function editAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['content_id'];
        /** @var int $currentContentTextId */
        $currentContentTextId = $params['id'];

        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\Form $form */
        $form = Form::get('page/ContentTextUpdate');
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        $form->setEntityId($currentContentTextId);
        /** create form */
        if ($form->isSubmit() && isset($params['title'])) {
            /** @var string $currentTitle */
            $currentTitle = strtolower($params['title']);

            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['display_title'], 0);
            $form->setFieldValue('title', $currentTitle);
            $form->setFieldValue('content_id', $currentContentId);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();

        if ($form->getIsValid() === true) {
            // add page update date
            $currentPageItem->setUpdatedAt($date)->save();
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'currentPageItem' => $currentPageItem,
            'currentContentId' => $currentContentId,
            'formErrors' => $formErrors,
            'formParams' => [
                'page_id' => $currentPageItem->getEntityId(),
                'content_id' => $currentContentId,
                'id' => $currentContentTextId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }
}
