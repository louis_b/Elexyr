<?php

namespace src\PageBundle\Controller;

use Template;
use Entity;
use Tools;
use OnyxDate;
use Form;
use Collection;
use app\Core\RouteManager;

/**
 * Class ContentHVideoController
 *
 * @package Elexyr\PageBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class ContentVideoController
{

    /** @var string CONTENT_TEXT_TYPE */
    const CONTENT_VIDEO = 'ContentVideo';

    /**
     * Add content type action
     * @param array $params
     */
    public function addAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['id'];
        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\Form $form */
        $form = Form::get('page/ContentVideoCreate');
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        /** create form */
        if ($form->isSubmit() === true) {
            // manage checkbox value
            /** @var \app\Facade\Tools $form */
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['autoplay'], 0);
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['controls'], 0);
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['muted'], 0);
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['looping'], 0);

            $form->setFieldValue('content_id', $currentContentId);
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();

        /* ------------------
           DELETE EXISTING CONTENT TYPE ASSOCIATED TO CURRENT CONTENT
        */
        /** @var \app\Orm\Entity $currentContent */
        $currentContent = Entity::get('Content')->loadById($currentContentId);
        /** @var null|\app\Orm\Entity $existingContentType */
        $existingContentType = null;
        if ($form->getIsValid() === true && !empty($currentContent->getContentTypeId()) && !empty($currentContent->getContentType())) {
            $existingContentType = Entity::get($currentContent->getContentType())->loadById($currentContent->getContentTypeId());
        }
        if ($existingContentType instanceof \app\Orm\Entity && $existingContentType->getEntityId()) {
            $existingContentType->delete();
        }

        /* ------------------
           MANAGE CONTENT DATA IS GLOBAL CONTENT OBJECT
        */
        if ($form->getIsValid() === true) {
            /** @var \app\Orm\Entity $createdContentVideo */
            $createdContentVideo = Collection::get(self::CONTENT_VIDEO)->getLastEntity();

            $currentContent->setContentTypeId($createdContentVideo->getEntityId());
            $currentContent->setContentType(self::CONTENT_VIDEO);
            $currentContent->setUpdatedAt($date);
            $currentContent->save();

            // add page update date
            $currentPageItem->setUpdatedAt($date)->save();

            // redirect on success
            RouteManager::redirectToRoute('content_add', ['page_id' => $currentPageId, 'id' => $currentContentId]);
        }

        // manage form errors
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'currentPageItem' => $currentPageItem,
            'currentContentId' => $currentContentId,
            'formErrors' => $formErrors,
            'form' => $form,
            'currentMediaVideo' => $form->getFieldValue('video'),
            'formParams' => [
                'page_id' => $currentPageItem->getEntityId(),
                'id' => $currentContentId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit content header full height action
     * @param array $params
     */
    public function editAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['content_id'];
        /** @var int $currentContentVideoId */
        $currentContentVideoId = $params['id'];

        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\Form $form */
        $form = Form::get('page/contentVideoEdit');
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        $form->setEntityId($currentContentVideoId);
        /** create form */
        if ($form->isSubmit()) {

            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['autoplay'], 0);
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['controls'], 0);
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['muted'], 0);
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['looping'], 0);

            $form->setFieldValue('content_id', $currentContentId);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();

        if ($form->getIsValid() === true) {
            // add page update date
            $currentPageItem->setUpdatedAt($date)->save();
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'currentPageItem' => $currentPageItem,
            'currentContentId' => $currentContentId,
            'formErrors' => $formErrors,
            'currentMediaVideo' => $form->getFieldValue('video'),
            'formParams' => [
                'page_id' => $currentPageItem->getEntityId(),
                'content_id' => $currentContentId,
                'id' => $currentContentVideoId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }
}