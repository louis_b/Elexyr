<?php

namespace src\PageBundle\Controller;

use Entity;
use OnyxDate;
use app\Core\RouteManager;
use Collection;
use Repository;
use Form;
use Tools;
use Template;

/**
 * Class SectionController
 *
 * @package Elexyr\PageBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class SectionController
{

    /**
     * Add section dynamically
     * @param array $params
     */
    public function addSectionAction(array $params)
    {
        /** @var int $pageId */
        $pageId = $params['id'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var int $sectionsNumber */
        $sectionsNumber = Collection::get('Section')
            ->filterBy('page_id', $pageId, '=')
            ->getSize();

        Entity::get('Section')
            ->setPageId($pageId)
            ->setCreatedAt($date)
            ->setUpdatedAt($date)
            ->setPosition($sectionsNumber)
            ->save();
        /** @var \app\Orm\Entity $lastSectionCreated */
        $lastSectionCreated = Collection::get('Section')->getLastEntity();

        if ($lastSectionCreated instanceof \app\Orm\Entity && $lastSectionCreated->getEntityId()){
            Entity::get('Content')->setPageId($pageId)
                ->setSectionId($lastSectionCreated->getEntityId())
                ->setPosition(0)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->save();
        }
        /** @var int $pageSectionsNumber */
        $pageSectionsNumber = Entity::get('Page')->loadById($pageId)->getSectionsNumber();
        Entity::get('Page')->loadById($pageId)
            ->setSectionsNumber($pageSectionsNumber + 1)
            ->setUpdatedAt($date)
            ->save();

        RouteManager::redirectToRoute('pages_custom', ['id' => $pageId]);
    }

    /**
     * Delete page section
     * @param array $params
     */
    public function deleteSectionAction(array $params)
    {
        if (!isset($params['id'])) {
            RouteManager::redirectToRoute('page_list');
        }
        /** @var int $sectionId */
        $sectionId = (int) $params['id'];
        /** @var int $pageId */
        $pageId = (int) Entity::get('Section')->loadById($sectionId)->getPageId();
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        Repository::get('Section')->deleteSection($sectionId);
        Repository::get('Content')->deleteContent($sectionId);

        Entity::get('Page')->loadById($pageId)->setUpdatedAt($date)->save();

        // redirect to navigation page
        RouteManager::redirectToRoute('pages_custom', ['id' => $pageId]);
    }

    /**
     * Update section position (drag and drop)
     * @param array $params
     *
     * @return bool
     */
    public function updateSectionPositionAction(array $params)
    {
        if ( !isset($params['old_value']) || !isset($params['new_value']) ){
            return false;
        }

        /** @var int $oldValue */
        $oldValue = $params['old_value'];
        /** @var int $oldId */
        $oldId = $params['old_id'];
        /** @var int $newValue */
        $newValue = $params['new_value'];
        /** @var int $newId */
        $newId = $params['new_id'];

        Entity::get('Section')->loadById($newId)->setPosition($newValue)->save();
        Entity::get('Section')->loadById($oldId)->setPosition($oldValue)->save();

        return true;
    }

    /**
     * Edit section global data
     * @param array $params
     */
    public function editAction(array $params)
    {
        if (!isset($params['section_id'])) {
            RouteManager::redirectToRoute('page_list');
        }
        /** @var int $currentSectionId */
        $currentSectionId = $params['section_id'];
        /** @var \app\Orm\Entity $currentSection */
        $currentSection = Entity::get('Section')->loadById($currentSectionId);
        /** @var int $currentPageId */
        $currentPageId = $currentSection->getPageId();
        /** @var \app\Orm\Entity $currentPage */
        $currentPage = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Core\Form $updateForm */
        $updateForm = Form::get('page/sectionEdit')->setEntityObject($currentSection);

        if ($updateForm->isSubmit() === true) {
            $updateForm->setFieldValue('updated_at', $date);
            $updateForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($updateForm, ['use_background_image'], 0);
        }
        // submit form
        $updateForm->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($updateForm);

        /** @var string $urlKey */
        $urlKey = $currentPage->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'currentSectionId' => $currentSectionId,
            'currentSection' => $currentSection,
            'updateForm' => $updateForm,
            'formErrors' => $formErrors,
            'currentPageId' => $currentPageId,
            'currentPage' => $currentPage,
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }
}
