<?php

namespace src\PageBundle\Controller;

use OnyxDate;
use Entity;
use Collection;
use Repository;
use Form;
use Tools;
use ErrorManager;
use Template;

/**
 * Class ContentController
 *
 * @package Elexyr\PageBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */

class ContentController
{

    /**
     * Create content element
     * @param array $params
     * @return bool
     */
    public function contentCreateAction(array $params)
    {
        if (!isset($params['content_number'])) {
            return false;
        }

        /** @var int $contentNumber */
        $contentNumber = $params['content_number'];
        /** @var int $pageId */
        $pageId = $params['page_id'];
        /** @var int $sectionId */
        $sectionId = $params['section_id'];
        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($pageId);

        /** @var string $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Orm\Collection $sectionContent */
        $sectionContent = Collection::get('Content')
            ->filterBy('page_id', $pageId, '=')
            ->filterBy('section_id', $sectionId, '=')
            ->getSize();

        /** when $sectionContent < $contentNumber */
        if ($sectionContent < $contentNumber){
            Repository::get('Content')->addSection($pageId, $sectionId, $contentNumber - $sectionContent);
            Entity::get('Section')->loadById($sectionId)
                ->setNbDivs($contentNumber)
                ->setUpdatedAt($date)
                ->save();

            // add page update date
            $currentPageItem->setUpdatedAt($date)->save();

            echo Tools::get('page/section')->contentItemsToJSON($sectionId);

            return false;
        }

        /** when $sectionContent > $contentNumber */
        if ($sectionContent > $contentNumber){
            Repository::get('Content')->deleteSectionNotUsed($pageId, $sectionId, $contentNumber);
            Entity::get('Section')->loadById($sectionId)
                ->setNbDivs($contentNumber)
                ->setUpdatedAt($date)
                ->save();

            // add page update date
            $currentPageItem->setUpdatedAt($date)->save();

            echo Tools::get('page/section')->contentItemsToJSON($sectionId);

            return false;
        }

        /** when $sectionContent == $contentNumber */
        if ($sectionContent == $contentNumber){
            echo Tools::get('page/section')->contentItemsToJSON($sectionId);

            return false;
        }

        for ($i = 1; $i <= $contentNumber; $i++){
            Entity::get('Content')->setPageId($pageId)
                ->setSectionId($sectionId)
                ->setPosition($i)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->save();
        }

        Entity::get('Section')->loadById($sectionId)
            ->setNbDivs($contentNumber)
            ->setUpdatedAt($date)
            ->save();

        // add page update date
        $currentPageItem->setUpdatedAt($date)->save();

        echo Tools::get('page/section')->contentItemsToJSON($sectionId);

        return true;
    }

    /**
     * display content selection page
     * @param array $params
     */
    public function contentAddAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['id'];

        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);
        /** @var \app\Orm\Entity $currentContent */
        $currentContent = Entity::get('Content')->loadById($currentContentId);
        /** @var array $allContentTypes */
        $allContentTypes = Tools::get('page/contentType')->getAllContentTypes();

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'currentPageItem' => $currentPageItem,
            'allContentTypes' => $allContentTypes,
            'currentContentId' => $currentContentId,
            'currentContentType' => $currentContent->getContentType(),
            'currentContent' => $currentContent,
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }
}