<?php

namespace src\PageBundle\Repository;

use app\Orm\Collection;
use Entity;
use app\Orm\Repository;

class SectionRepository extends Repository
{

    public function deleteSection($sectionId)
    {
        /** @var \app\Orm\Entity $section */
        $section = Entity::get($this->getRepositoryIdentifier())->loadById($sectionId);
        /** @var int $pageId */
        $pageId = (int) $section->getPageId();
        $pageSectionsNumber = Entity::get('Page')->loadById($pageId)->getSectionsNumber();
        Entity::get('Page')->loadById($pageId)->setSectionsNumber($pageSectionsNumber - 1)->save();

        $section->delete();

        $pageSections = Collection::get($this->getRepositoryIdentifier())
            ->filterBy('page_id', $pageId, '=')
            ->sortBy('position', 'ASC')
            ->all();

        $i = 0;
        foreach ($pageSections as $pageSection) {
            $pageSection->setPosition($i);
            $pageSection->save();
            $i++;
        }
    }

    public function deleteAllSection($pageId)
    {
        $this->delete()->where([
            ['page_id', '=', $pageId],
        ]);
    }

}