<?php

namespace src\PageBundle\Repository;

use Repository;
use Collection;
use Entity;
use OnyxDate;

class ContentRepository extends \app\Orm\Repository
{

    /**
     * add section
     * @param int $pageId
     * @param int $sectionId
     * @param int $contentToAdd
     */
    public function addSection($pageId, $sectionId, $contentToAdd){

        /** @var string $date */
        $date = OnyxDate::get()->toString();

        for ($i = 1; $i <= $contentToAdd; $i++){
            Entity::get('Content')->setPageId($pageId)
                ->setSectionId($sectionId)
                ->setPosition($i)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->save();
        }

        $contentCollections = Collection::get($this->getRepositoryIdentifier())
            ->filterBy('page_id', $pageId, '=')
            ->filterBy('section_id', $sectionId, '=')
            ->sortBy('entity_id', 'ASC')
            ->all();

        foreach ($contentCollections as $key => $contentCollection) {
            $contentCollection->setPosition($key);
            $contentCollection->save();
        }
    }

    /**
     * delete
     * @param int $pageId
     * @param int $sectionId
     * @param int $itemsNb
     */
    public function deleteSectionNotUsed($pageId, $sectionId, $itemsNb)
    {
        $this->delete()->where([
            ['page_id', '=', $pageId],
            ['section_id', '=', $sectionId],
            ['position', '>=', $itemsNb],
        ]);

        $contentCollections = Collection::get($this->getRepositoryIdentifier())
            ->filterBy('page_id', $pageId, '=')
            ->filterBy('section_id', $sectionId, '=')
            ->sortBy('position', 'ASC')
            ->all();

        $i = 0;
        foreach ($contentCollections as $contentCollection) {
            $contentCollection->setPosition($i);
            $contentCollection->save();
            $i++;
        }

    }

    /**
     * delete one content item
     * @param int $sectionId
     */
    public function deleteContent($sectionId)
    {
        $this->delete()->where([
            ['section_id', '=', $sectionId],
        ]);
    }

    /**
     * delete all content items
     * @param int $pageId
     */
    public function deleteAllContent($pageId)
    {
        /** @var array $contentTypesData */
        $contentTypesData = $this->select()->columns([
            'content_type_id',
            'content_type'
        ])->where([
            ['page_id', '=', $pageId],
            ['content_type_id', '>', 0]
        ]);
        // delete content objects
        $this->delete()->where([
            ['page_id', '=', $pageId],
        ]);
        // delete content types objects
        foreach ($contentTypesData as $contentTypeToDelete) {
            if (!empty($contentTypeToDelete['content_type_id']) && !empty($contentTypeToDelete['content_type'])) {
                Repository::get($contentTypeToDelete['content_type'])->delete()->where([
                    ['entity_id', '=', $contentTypeToDelete['content_type_id']]
                ]);
            }
        }
    }

    /**
     * get ids of content section by sectionId
     * @param int $sectionId
     * @return \app\Orm\Query\Select|array
     */
    public function getSectionContentIds($sectionId)
    {
        return $this->select()->columns(['entity_id'])->where([
            ['section_id', '=', $sectionId]
        ]);
    }
}