<?php

namespace src\PageBundle\Repository;

use Repository;

/**
 * Class PageRepository
 *
 * @package Onyx\PageBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class PageRepository extends \app\Orm\Repository
{

    /**
     * Get default url key value for a page
     * @param string $pageName
     *
     * @return string
     */
    public function getDefaultUrlKey($pageName)
    {
        /** @var string $pageName */
        $pageName = trim($pageName);
        $pageName = strtolower($pageName);
        $pageName = str_replace(' ', '-', $pageName);

        /** @var int $i */
        $i = 0;
        /** @var string $searchedUrlKey */
        $searchedUrlKey = $pageName;
        do {
            /** @var int $alreadyExists */
            $alreadyExists = $this->select()->count()->where([
                ['url_key', '=', $searchedUrlKey]
            ]);
            $alreadyExists = array_values($alreadyExists[0]);
            $alreadyExists = $alreadyExists[0];

            // increment url key name
            $i++;
            $searchedUrlKey = $pageName . '-' . $i;
        } while ($alreadyExists > 0);

        if ($i == 1) {
            return $pageName;
        }

        return $pageName . '-' . ($i - 1);
    }

    /**
     * Mass delete method by ids
     * @param array $idsToDelete
     */
    public function massDelete(array $idsToDelete)
    {
        /** @var SectionRepository $sectionRepository */
        $sectionRepository = Repository::get('Section');
        /** @var ContentRepository $contentRepository */
        $contentRepository = Repository::get('Content');

        foreach ($idsToDelete as $id) {
            $this->delete()->where([
                ['entity_id', '=', $id]
            ]);

            $sectionRepository->deleteAllSection($id);
            $contentRepository->deleteAllContent($id);
        }
    }
}
