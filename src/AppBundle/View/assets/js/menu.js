var Menu = {

    toggle: function (toggleMenu,toggleButton) {

        toggleMenu = $(toggleMenu);
        toggleButton = $(toggleButton);

        $(window).resize(function () {
            if ($(this).width() > 768) {
                toggleMenu.show();
            }
        });

        toggleButton.on("click", function () {
                if (toggleMenu.is(':animated')) {
                    return false
                }
                else {
                    toggleMenu.slideToggle();
                }
            })
        }
};