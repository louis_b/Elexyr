var Checkbox = {

    visibleButton : function(buttonVisible,buttonNotVisible) {

        buttonVisible = jQuery(buttonVisible);
        buttonNotVisible = jQuery(buttonNotVisible);

        buttonVisible.on('click', function () {
            $(this).hide();
            $(this).parent().parent().find(buttonNotVisible).show();
            $(this).closest('.page').addClass('not-visible');
        });

        buttonNotVisible.on('click',function () {
            $(this).hide();
            $(this).parent().parent().find(buttonVisible).show();
            $(this).closest('.page').removeClass('not-visible');
        });
    },

    openInput : function(button,buttonSave,form){

        button = jQuery(button);
        buttonSave = jQuery(buttonSave);
        form = jQuery(form);
        // test if element is a form
        if (!form.is('form')) {
            return false;
        }

        var input = form.find('.input-text');
        var submit = form.find('.input-submit');

        button.on('click',function () {

            if ($(input).val().length > 0){
                button.hide();
                buttonSave.show();
            }

            if (input.hasClass('open')){
                form.removeClass('open');
                input.removeClass('open');
                return false;
            } else {

                form.addClass('open');
                input.addClass('open');

                input.on('keyup',function() {

                    button.hide();
                    buttonSave.show();

                });

                input.on('blur',function (e) {

                    if (e.relatedTarget === buttonSave.get()[0]){
                        e.preventDefault();
                        input.focus();
                        submit.trigger('click');

                        return false;
                    }

                    buttonSave.hide();
                    button.show();
                    form.removeClass('open');
                    input.removeClass('open');
                })

            }

        })
    },

    checkboxSelector: function(ctaCheckboxUnchecked,ctaCheckboxChecked,checkboxUnchecked,checkboxChecked,checkboxInput){

        ctaCheckboxUnchecked = jQuery(ctaCheckboxUnchecked);
        ctaCheckboxChecked = jQuery(ctaCheckboxChecked);
        checkboxUnchecked = jQuery(checkboxUnchecked);
        checkboxChecked = jQuery(checkboxChecked);
        checkboxInput = jQuery(checkboxInput);

        var allCheckbox = (checkboxInput.length);

        ctaCheckboxUnchecked.on("click",function () {
            ctaCheckboxUnchecked.hide();
            ctaCheckboxChecked.show();

            checkboxUnchecked.hide();
            checkboxChecked.show();
            checkboxInput.attr( 'checked', true );
        });
        ctaCheckboxChecked.on("click",function () {
            ctaCheckboxUnchecked.show();
            ctaCheckboxChecked.hide();

            checkboxChecked.hide();
            checkboxUnchecked.show();
            checkboxInput.attr( 'checked', false );
        });
        checkboxUnchecked.each(function() {
            $(this).on("click",function () {
                $(this).hide();
                $(this).parent().find(checkboxChecked).show();
                $(this).parent().find(checkboxInput).attr( 'checked', true );

                if ($("input.checkbox-input:checkbox:checked").length === allCheckbox){
                    ctaCheckboxUnchecked.hide();
                    ctaCheckboxChecked.show();
                }

            });
        });
        checkboxChecked.each(function() {
            $(this).on("click",function () {
                ctaCheckboxChecked.hide();
                ctaCheckboxUnchecked.show();

                $(this).hide();
                $(this).parent().find(checkboxUnchecked).show();
                $(this).parent().find(checkboxInput).attr( 'checked', false );
            });
        });
    },

    imgInput: function (addButton,previewImg) {

        addButton = $(addButton);
        previewImg = $(previewImg);


        addButton.on('click',function () {
            var inputImg = $(this).closest('div').find('input[type=file]');
            inputImg.trigger('click');

            inputImg.change(function(){
                var currentItem = $(this);
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        currentItem.closest('div').find(previewImg).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(this.files[0]);
                }
            });

        })

    },

    toggleTextOnSwitchButton: function (switchButton, textAfterClick) {
        switchButton = jQuery(switchButton);
        var switchButtonCheckbox = switchButton.find('input[type=checkbox]');
        var textToChange = switchButton.parent().find('p');
        var textBeforeClick = switchButton.parent().find('p').html();

        if (switchButtonCheckbox.is(':checked')){
            textToChange.html(textAfterClick);
            switchButton.parent().show();
        } else {
            switchButton.parent().show();
        }

        switchButton.on('click',function () {
            if (switchButtonCheckbox.is(':checked')){
                textToChange.html(textAfterClick);
            } else {
                textToChange.html(textBeforeClick);
            }
        })
    }

};
