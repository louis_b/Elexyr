var Toggle = {

    toggleContent: function (trigger, content) {
        trigger = jQuery(trigger);
        content = jQuery(content);

        trigger.on('click', function () {
            if (content.hasClass('open')) {
                content.removeClass('open');

                return null;
            }

            content.addClass('open');
        })
    }
};
