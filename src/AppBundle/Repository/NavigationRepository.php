<?php

namespace src\AppBundle\Repository;

use app\Orm\Repository;

class NavigationRepository extends Repository
{

    /**
     *
     */
    public function resetIsDefault()
    {
        $this->update()->set(["is_default" => 0])->where();
    }
}
