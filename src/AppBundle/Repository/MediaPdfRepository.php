<?php

namespace src\AppBundle\Repository;

use app\Orm\Repository;

class MediaPdfRepository extends Repository
{

    /**
     * Mass delete method by ids
     * @param array $idsToDelete
     */
    public function massDelete(array $idsToDelete)
    {
        foreach ($idsToDelete as $id) {
            $this->delete()->where([
                ['entity_id', '=', $id]
            ]);
        }
    }
}
