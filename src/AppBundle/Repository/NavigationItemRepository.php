<?php

namespace src\AppBundle\Repository;

use app\Orm\Repository;

class NavigationItemRepository extends Repository
{

    /**
     *  delete all items for specific navigation
     * @param int $navigationId
     */
    public function deleteAllNavigationItems($navigationId)
    {
        $this->delete()->where([
            ['navigation_id', '=', $navigationId],
        ]);
    }
}
