<?php

namespace src\AppBundle\Controller;

use app\Core\RouteManager;
use app\Orm\Query\Select;
use Collection;
use Template;
use OnyxDate;
use Form;
use Tools;
use Entity;

/**
 * Class NavigationController
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class MediaController
{

    /**
     * Create media image action
     * @param array $params
     */
    public function createImageAction(array $params)
    {
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaImageCreate');
        if ($form->isSubmit() === true) {
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'formActionParams' => []
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit media image action
     * @param array $params
     */
    public function editImageAction(array $params)
    {
        if (!isset($params['image_id'])) {
            RouteManager::redirectToRoute('media_list');
        }
        /** @var int $imageId */
        $imageId = (int) $params['image_id'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Orm\Entity $image */
        $image = Entity::get('MediaImage')->loadById($imageId);
        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaImageUpdate')->setEntityObject($image);
        if ($form->isSubmit() === true) {
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'imageId' => $imageId,
            'imageObject' => $image,
            'formActionParams' => ['image_id' => $imageId]
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Create media pictogram action
     * @param array $params
     */
    public function createPictogramAction(array $params)
    {
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaPictogramCreate');
        if ($form->isSubmit() === true) {
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'formActionParams' => []
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit media pictogram action
     * @param array $params
     */
    public function editPictogramAction(array $params)
    {
        if (!isset($params['pictogram_id'])) {
            RouteManager::redirectToRoute('media_list');
        }
        /** @var int $pictogramId */
        $pictogramId = (int) $params['pictogram_id'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Orm\Entity $pictogram */
        $pictogram = Entity::get('MediaPictogram')->loadById($pictogramId);
        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaPictogramUpdate')->setEntityObject($pictogram);
        if ($form->isSubmit() === true) {
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'pictogramId' => $pictogramId,
            'pictogramObject' => $pictogram,
            'formActionParams' => ['pictogram_id' => $pictogramId]
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Create media pdf action
     * @param array $params
     */
    public function createPdfAction(array $params)
    {
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaPdfCreate');
        if ($form->isSubmit() === true) {
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'formActionParams' => []
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit media pdf action
     * @param array $params
     */
    public function editPdfAction(array $params)
    {
        if (!isset($params['pdf_id'])) {
            RouteManager::redirectToRoute('media_list');
        }
        /** @var int $pdfId */
        $pdfId = (int) $params['pdf_id'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Orm\Entity $pdf */
        $pdf = Entity::get('MediaPdf')->loadById($pdfId);
        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaPdfUpdate')->setEntityObject($pdf);
        if ($form->isSubmit() === true) {
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'pdfId' => $pdfId,
            'pdfObject' => $pdf,
            'formActionParams' => ['pdf_id' => $pdfId]
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Create media video action
     * @param array $params
     */
    public function createVideoAction(array $params)
    {
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaVideoCreate');
        if ($form->isSubmit() === true) {
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'formActionParams' => []
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit media video action
     * @param array $params
     */
    public function editVideoAction(array $params)
    {
        if (!isset($params['video_id'])) {
            RouteManager::redirectToRoute('media_list');
        }
        /** @var int $videoId */
        $videoId = (int) $params['video_id'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Orm\Entity $video */
        $video = Entity::get('MediaVideo')->loadById($videoId);
        /** @var \app\Core\Form $form */
        $form = Form::get('app/mediaVideoUpdate')->setEntityObject($video);
        if ($form->isSubmit() === true) {
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'videoId' => $videoId,
            'videoObject' => $video,
            'formActionParams' => ['video_id' => $videoId]
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * List all medias by type action
     * @param array $params
     */
    public function listAction(array $params)
    {
        /** @var \src\AppBundle\Tools\Media $mediaHelper */
        $mediaHelper = Tools::get('app/media');
        if (!isset($params['type']) || $mediaHelper->isValidType($params['type']) !== true) {
            $params['type'] = 'image';
        }

        // manage pagination
        /** @var int $pageNumber */
        $pageNumber = 1;
        if (isset($params['page_number']) && $params['page_number'] > 1) {
            $pageNumber = (int) $params['page_number'];
        }

        /** @var string $mediaType */
        $mediaType = $params['type'];
        /** @var string $mediaIdentifier */
        $mediaIdentifier = $mediaHelper->getEntityIdentifierByType($mediaType);
        /** @var \src\UserBundle\Tools\Page $pageHelper */
        $pageHelper = Tools::get('app/page')->setPageEntity($mediaIdentifier);

        /** @var \app\Orm\Collection $mediaCollection */
        $mediaCollection = Collection::get($mediaIdentifier);
        $mediaCollection = $pageHelper->addLimitToCollection($mediaCollection, $pageNumber);
        $mediaCollection = $mediaCollection->sortBy('updated_at', Select::SORT_ORDER_DESC)->all();

        // if collection is empty redirect to first page
        if (count($mediaCollection) <= 0 && $pageNumber != 1) {
            RouteManager::redirectToRoute('media_list');
        }
        /** @var int|bool $previousPage */
        $previousPage = $pageHelper->getPreviousPage($pageNumber);
        /** @var int|bool $nextPage */
        $nextPage = $pageHelper->getNextPage($pageNumber);

        /** @var array $viewParams */
        $viewParams = [
            'mediaCollection' => $mediaCollection,
            'mediaHelper' => $mediaHelper,
            'mediaType' => $mediaType,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * List all medias action
     * @param array $params
     */
    public function listAllAction(array $params)
    {
        /** @var \src\AppBundle\Tools\Media $mediaHelper */
        $mediaHelper = Tools::get('app/media');

        // manage pagination
        /** @var int $pageNumber */
        $pageNumber = 1;
        if (isset($params['page_number']) && $params['page_number'] > 1) {
            $pageNumber = (int) $params['page_number'];
        }
        /** @var array $mixedCollection */
        $mixedCollection = $mediaHelper->getMixedCollection($pageNumber);

        // if collection is empty redirect to first page
        if (count($mixedCollection) <= 0 && $pageNumber != 1) {
            RouteManager::redirectToRoute('media_list');
        }

        /** @var \src\AppBundle\Tools\Page $pageHelper */
        $pageHelper = Tools::get('app/page');
        /** @var int|bool $previousPage */
        $previousPage = $pageHelper->getPreviousPage($pageNumber);
        /** @var int|bool $nextPage */
        $nextPage = $pageHelper->getMixedNextPage($pageNumber);

        /** @var array $viewParams */
        $viewParams = [
            'mediaCollection' => $mixedCollection,
            'mediaHelper' => $mediaHelper,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Delete media item
     * @param array $params
     */
    public function deleteAction(array $params)
    {
        /** @var \src\AppBundle\Tools\Media $mediaHelper */
        $mediaHelper = Tools::get('app/media');
        if (!isset($params['id']) || $mediaHelper->isValidType($params['type']) !== true) {
            RouteManager::redirectToRoute('media_list');
        }

        /** @var int $mediaId */
        $mediaId = $params['id'];
        /** @var string $mediaType */
        $mediaType = $params['type'];
        /** @var string $mediaIdentifier */
        $mediaIdentifier = $mediaHelper->getEntityIdentifierByType($mediaType);
        /** @var \app\Orm\Entity $mediaToDelete */
        $mediaToDelete = Entity::get($mediaIdentifier)->loadById($mediaId);
        $mediaToDelete->delete();

        // redirect to media list page
        RouteManager::redirectToRoute('media_list');
    }

    /**
     * Mass delete for medias
     * @param array $params
     */
    public function massDeleteAction(array $params)
    {
        /** @var array $mediaIdsToDelete */
        $mediaIdsToDelete = [];
        /** @var \src\AppBundle\Tools\Media $mediaHelper */
        $mediaHelper = Tools::get('app/media');
        /** @var array $availableMedias */
        $availableMedias = $mediaHelper::AVAILABLE_MEDIA_TYPES;
        // format ids to delete by entities
        foreach ($params as $key => $param) {
            foreach ($availableMedias as $mediaType) {
                if (strpos($key, $mediaType) !== false) {
                    $mediaIdsToDelete[$mediaType][] = str_replace($mediaType . '-', '', $key);
                }
            }
        }
        // delete items for each media types
        foreach ($availableMedias as $mediaType) {
            if (isset($mediaIdsToDelete[$mediaType])) {
                $mediaHelper->massMediaDelete($mediaType, $mediaIdsToDelete[$mediaType]);
            }
        }

        RouteManager::redirectToRoute('media_list');
    }
}
