<?php

namespace src\AppBundle\Controller;

use app\Core\RouteManager;
use app\Orm\Query\Select;
use Request;
use Template;
use Form;
use OnyxDate;
use Collection;
use Entity;
use Tools;
use Repository;

/**
 * Class NavigationController
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class NavigationController
{

    /**
     * Display navigation page
     * @param array $params
     */
    public function navigationAction(array $params)
    {
        /** @var int $pageNumber */
        $pageNumber = 1;
        if (isset($params['page_number']) && $params['page_number'] > 1) {
            $pageNumber = (int) $params['page_number'];
        }

        /** @var \app\Core\Form $form */
        $form = Form::get('app/navigationCreate');
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        if ($form->isSubmit() === true) {
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var \src\UserBundle\Tools\Page $pageHelper */
        $pageHelper = Tools::get('app/pageNavigation');
        /** @var \app\Orm\Collection $navigationCollection */
        $navigationCollection = Collection::get('Navigation');
        $navigationCollection = $pageHelper->addLimitToCollection($navigationCollection, $pageNumber);
        /** @var array $navigationCollection */
        $navigationCollection = $navigationCollection->sortBy('updated_at', Select::SORT_ORDER_DESC)->all();
        // if collection is empty redirect to first page
        if (count($navigationCollection) <= 0 && $pageNumber != 1) {
            RouteManager::redirectToRoute('navigation');
        }

        /** @var int|bool $previousPage */
        $previousPage = $pageHelper->getPreviousPage($pageNumber);
        /** @var int|bool $nextPage */
        $nextPage = $pageHelper->getNextPage($pageNumber);

        /** @var array $viewParams */
        $viewParams = [
            'creationForm' => $form,
            'navigationCollection' => $navigationCollection,
            'formErrors' => $formErrors,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage
        ];
        // render view
        Template::get()->setParams($viewParams);
    }

    /**
     * @param array $params
     */
    public function deleteAction(array $params)
    {
        /** @var int $navigationId */
        $navigationId = $params['id'];

        Entity::get('Navigation')->loadById($navigationId)->delete();
        // delete navigation items associated to the navigation to delete
        Repository::get('NavigationItem')->deleteAllNavigationItems($navigationId);

        // redirect to navigation page
        RouteManager::redirectToRoute('navigation');
    }

    /**
     * @param array $params
     */
    public function itemDeleteAction(array $params)
    {
        /** @var int $navigationId */
        $navigationItemId = $params['id'];
        /** @var \app\Orm\Entity $navItemToDelete */
        $navItemToDelete = Entity::get('NavigationItem')->loadById($navigationItemId);
        /** @var int $navigationId */
        $navigationId = $navItemToDelete->getNavigationId();

        $navItemToDelete->delete();

        // redirect to navigation page
        RouteManager::redirectToRoute('navigation_edit', ['id' => $navigationId ]);
    }

    /**
     * @param array $params
     */
    public function updateStatusAction(array $params)
    {
        /** @var int $navigationId */
        $navigationId = (int)$params['id'];
        /** @var \app\Orm\Entity $currentNavigation */
        $currentNavigation = Entity::get('Navigation')->loadById($navigationId);
        /** @var boolean $navigationIsDefault */
        $navigationIsDefault = $currentNavigation->getIsDefault();
        $navigationIsDefault = Tools::get('app/attributes')->getBooleanOppositeValue($navigationIsDefault);
        // reset all is default values
        Repository::get('Navigation')->resetIsDefault();
        // add is default value for current navigation
        $currentNavigation->setIsDefault($navigationIsDefault)->save();

        // redirect to navigation page
        RouteManager::redirectToRoute('navigation');
    }

    /**
     * Display navigation_edit page
     * @param array $params
     */
    public function navigationEditAction(array $params)
    {
        /** @var int|null $updateItemId */
        $updateItemId = null;
        if (isset($params['updateItemId'])) {
            $updateItemId = $params['updateItemId'];
        }
        // init form errors
        $formErrors = [];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /* ------------------
           MANAGE CREATE FORM
        */
        /** @var \app\Core\Form $itemCreationForm */
        $itemCreationForm = Form::get('app/navigationItemCreate');

        if (empty($updateItemId) && $itemCreationForm->isSubmit() === true && isset($params['title'])) {
            /** @var string $urlTarget */
            $urlTarget = Tools::get('page/urlKey')->generateValidUrlKey($params['title']);

            $itemCreationForm->setFieldValue('link_target', $urlTarget);
            $itemCreationForm->setFieldValue('created_at', $date);
            $itemCreationForm->setFieldValue('updated_at', $date);
            $itemCreationForm->setFieldValue('navigation_id', $params['id']);
        }
        if (empty($updateItemId)) {
            // submit form
            $itemCreationForm->submit();
            // manage form failures for creation form
            $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($itemCreationForm);
        }
        /* ------------------ */

        /* ------------------
           MANAGE UPDATE FORMS
        */
        /** @var \app\Core\Form $itemUpdateForm */
        $itemUpdateForm = Form::get('app/navigationItemUpdate');

        if (!empty($updateItemId) && $itemUpdateForm->isSubmit() === true) {
            $itemUpdateForm->setFieldValue('updated_at', $date);
        }
        if (!empty($updateItemId)) {
            $itemUpdateForm->setEntityId($updateItemId);
            $itemUpdateForm->submit();
            // manage form failures for update form that was submit
            $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($itemUpdateForm);
        }

        /** @var \app\Orm\Collection $navigationItemCollection */
        $navigationItemCollection = Collection::get('NavigationItem')
            ->filterBy('navigation_id', $params['id'], "=")
            ->sortBy('position')
            ->all();

        /** @var array $updateForms */
        $updateForms = [];
        foreach ($navigationItemCollection as $item) {
            if ($updateItemId != $item->getEntityId()) {
                /** @var \app\Core\Form $itemForm */
                $itemForm = Form::get('app/navigationItemUpdate')->setEntityObject($item)->presetEntityData();
            } else {
                $itemForm = $itemUpdateForm;
            }
            // add form to collection
            $updateForms[$item->getEntityId()] = $itemForm;
        }
        /* ------------------ */

        /* ------------------
           SEND VIEW PARAMS
        */
        /** @var string $currentNavigationName */
        $currentNavigationName = Entity::get('Navigation')->loadById($params['id'])->getName();
        /** @var array $templateParams */
        $templateParams = [
            'currentNavigationName' => $currentNavigationName,
            'creationForm' => $itemCreationForm,
            'currentNavigationId' => $params['id'],
            'updateItemId' => $updateItemId,
            'navigationItemCollection' => $navigationItemCollection,
            'updateFormsCollection' => $updateForms,
            'formErrors' => $formErrors
        ];

        Template::get()->setParams($templateParams);
    }

    /**
     * Display navigation_custom page
     * @param array $params
     */
    public function navigationCustomAction(array $params)
    {
        /** @var string $currentNavigationName */
        $currentNavigation = Entity::get('Navigation')->loadById($params['id']);
        /** @var string $currentNavigationName */
        $currentNavigationName = $currentNavigation->getName();
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var \app\Core\Form $itemUpdateForm */
        $itemUpdateForm = Form::get('app/navigationCustomUpdate')->setEntityObject($currentNavigation);
        if ($itemUpdateForm->isSubmit() === true) {
            $itemUpdateForm->setFieldValue('updated_at', $date);
            $itemUpdateForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($itemUpdateForm, ['orientation'], 0);
            $itemUpdateForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($itemUpdateForm, ['text_decoration'], 0);
        }
        $itemUpdateForm->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($itemUpdateForm);

        /** @var array $templateParams */
        $templateParams = [
            'updateForm' => $itemUpdateForm,
            'currentNavigationId' => $params['id'],
            'currentNavigationName' => $currentNavigationName,
            'formErrors' => $formErrors,
        ];

        Template::get()->setParams($templateParams);
    }

    /**
     * Display navigation_custom page
     * @param array $params
     */
    public function navigationItemCustomAction(array $params)
    {
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();

        /** @var int $currentNavigationItemId */
        $currentNavigationId = $params['id'];
        /** @var int $currentNavigationItemId */
        $currentNavigationItemId = $params['navigation_item_id'];
        /** @var string $currentNavigationName */
        $currentNavigationName = Entity::get('Navigation')->loadById($currentNavigationId)->getName();
        /** @var \app\Orm\Entity $currentNavigationItem */
        $currentNavigationItem = Entity::get('NavigationItem')->loadById($currentNavigationItemId);

        /** @var \app\Core\Form $itemUpdateForm */
        $itemUpdateForm = Form::get('app/navigationItemUpdateCustom')->setEntityObject($currentNavigationItem);
        if ($itemUpdateForm->isSubmit() === true) {
            $itemUpdateForm->setFieldValue('updated_at', $date);
            $itemUpdateForm = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($itemUpdateForm, ['use_active_background_color', 'use_hover_background_color'], 0);
        }
        $itemUpdateForm->submit();
        // manage form failures for update form that was submit
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($itemUpdateForm);

        /** @var array $templateParams */
        $templateParams = [
            'updateForm' => $itemUpdateForm,
            'currentNavigationId' => $params['id'],
            'currentNavigationItem' => $currentNavigationItem,
            'currentNavigationItemId' => $params['navigation_item_id'],
            'currentNavigationName' => $currentNavigationName,
            'formErrors' => $formErrors
        ];

        Template::get()->setParams($templateParams);
    }

}
