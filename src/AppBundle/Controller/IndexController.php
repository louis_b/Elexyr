<?php

namespace src\AppBundle\Controller;

use app\Core\RouteManager;
use app\Orm\Query\Select;
use Request;
use Template;
use Collection;
use Tools;

/**
 * Class IndexController
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class IndexController
{

    /**
     * Display home page
     * @param array $params
     */
    public function homeAction(array $params)
    {

        /** @var int $userNumber */
        $userNumber = Collection::get('User')->getSize();
        /** @var array $usersRoles */
        $usersRoles = Tools::get('user/roles')->getAvailableRoles();
        /** @var array $roleCounts */
        $roleCounts = [];
        foreach ($usersRoles as $roleCode => $roleLabel) {
            $roleCounts[$roleLabel] = Collection::get('User')->filterBy('role', $roleCode)->getSize();
        }

        /** @var inr $pageNumber */
        $pageNumber = Collection::get('Page')->getSize();
        /** @var int $mediaNumber */
        $mediaNumber = 0;
        $mediaNumber += Collection::get('MediaImage')->getSize();
        $mediaNumber += Collection::get('MediaPictogram')->getSize();
        $mediaNumber += Collection::get('MediaPdf')->getSize();
        $mediaNumber += Collection::get('MediaVideo')->getSize();
        /** @var int $pluginNumber */
        $pluginNumber = Tools::get('plugin/data')->getPluginsCount();

        /** @var array $recentlyEditedPages */
        $recentlyEditedPages = Collection::get('Page')
            ->sortBy('updated_at', Select::SORT_ORDER_DESC)
            ->addLimit(7)
            ->all();

        /** @var string $currentUserName */
        $currentUser = Tools::get('user/user')->getCurrentUser();
        /** @var bool $isNight */
        $isNight = Tools::get('app/time')->isNight();

        /** @var array $viewParams */
        $viewParams = [
            'userNumber' => $userNumber,
            'pageNumber' => $pageNumber,
            'mediaNumber' => $mediaNumber,
            'pluginNumber' => $pluginNumber,
            'currentUser' => $currentUser,
            'isNight' => $isNight,
            'recentlyEditedPages' => $recentlyEditedPages,
            'frontendHelper' => Tools::get('frontend/router'),
            'roleCounts' => $roleCounts
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Search results action
     * @param array $params
     */
    public function searchAction(array $params)
    {
        if (!isset($params['search_query'])) {
            RouteManager::redirectToRoute('home');
        }
        /** @var string $searchQuery */
        $searchQuery = $params['search_query'];
        $searchQuerySql = '%' . $searchQuery . '%';

        /** @var array $pageResults */
        $pageResults = Collection::get('Page')
            ->filterBy('name', $searchQuerySql, 'LIKE')
            ->filterOr('url_key', $searchQuerySql, 'LIKE')
            ->all();
        /** @var array $userResults */
        $userResults = Collection::get('User')
            ->filterBy('firstname', $searchQuerySql, 'LIKE')
            ->filterOr('lastname', $searchQuerySql, 'LIKE')
            ->filterOr('pseudo', $searchQuerySql, 'LIKE')
            ->filterOr('email', $searchQuerySql, 'LIKE')
            ->filterOr('role', $searchQuerySql, 'LIKE')
            ->all();

        /** @var \src\AppBundle\Tools\Media $mediaHelper */
        $mediaHelper = Tools::get('app/media');
        /** @var array $mediaResults */
        $mediaResults = [];
        /** @var int $mediaCount */
        $mediaCount = 0;
        foreach ($mediaHelper::AVAILABLE_MEDIA_TYPES as $mediaType) {
            /** @var string $mediaEntityIdentifier */
            $mediaEntityIdentifier = $mediaHelper->getEntityIdentifierByType($mediaType);
            // add results for each media type
            $mediaResults[$mediaType] = Collection::get($mediaEntityIdentifier)
                ->filterBy('label', $searchQuerySql, 'LIKE')
                ->all();

            $mediaCount += count($mediaResults[$mediaType]);
        }

        /** @var array $pluginResults */
        $pluginResults = [];
        /** @var array $availablePlugins */
        $availablePlugins = Tools::get('plugin/data')->getAllPlugins();
        foreach ($availablePlugins as $pluginCode => $plugin) {
            if (strpos($pluginCode, $searchQuery) !== false || isset($plugin['name']) && strpos($plugin['name'], $searchQuery) !== false) {
                // set code in plugin data
                $plugin['code'] = $pluginCode;
                $pluginResults[] = $plugin;
            }
        }

        /** @var array $viewParams */
        $viewParams = [
            'searchQuery' => $searchQuery,
            'pageResults' => $pageResults,
            'userResults' => $userResults,
            'mediaResults' => $mediaResults,
            'pluginResults' => $pluginResults,
            'nbPages' => count($pageResults),
            'nbUsers' => count($userResults),
            'nbMedias' => $mediaCount,
            'nbPlugins' => count($pluginResults),
            'userImageHelper' => Tools::get('user/image'),
            'userRolesHelper' => Tools::get('user/roles')
        ];
        Template::get()->setParams($viewParams);
    }
}
