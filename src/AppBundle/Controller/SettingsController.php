<?php

namespace src\AppBundle\Controller;

use app\Core\RouteManager;
use Entity;
use Request;
use Template;
use Tools;
use OnyxDate;
use Form;

/**
 * Class SettingsController
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class SettingsController
{

    /**
     * Display settings page
     * @param array $params
     */
    public function settingsAction(array $params)
    {
        /** @var array $settings */
        $settings = Tools::get('app/setting')->getSettings();
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Core\Form $updateForm */
        $updateForm = Form::get('app/settings');

        if ($updateForm->isSubmit() === false) {
            // prefill settings values
            foreach ($settings as $settingCode => $setting) {
                /** @var string $settingValue */
                $settingValue = Entity::get('Setting')->loadBy('code', $settingCode)->getValue();
                if (!empty($settingValue)) {
                    // add value to form
                    $updateForm->setFieldValue($settingCode, $settingValue);
                }
            }
        }
        // submit form
        $updateForm->submit();
        // manage empty form
        if (!isset($params['website_name'])) {
            $updateForm->setIsValid(false);
        }

        if ($updateForm->isSubmit() === true && $updateForm->getIsValid() === true) {
            // set value to configuration
            foreach ($settings as $settingCode => $setting) {
                Entity::get('Setting')->loadBy('code', $settingCode)
                    ->setCode($settingCode)
                    ->setValue($updateForm->getFieldValue($settingCode))
                    ->setUpdatedAt($date)
                    ->save();
            }
            // redirect on success
            RouteManager::redirectToRoute('settings');
        }
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($updateForm);

        /** @var array $viewParams */
        $viewParams = [
            'settings' => $settings,
            'updateForm' => $updateForm,
            'formErrors' => $formErrors
        ];
        Template::get()->setParams($viewParams);
    }
}
