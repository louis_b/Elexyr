<?php

namespace src\AppBundle\Controller\Error;

use Template;

/**
 * Class Error404Controller
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Error404Controller
{

    /**
     * Method call for 404 page of AppBundle
     */
    public function indexAction()
    {
        Template::get('app/page404');
    }
}
