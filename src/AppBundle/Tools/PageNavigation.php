<?php

namespace src\AppBundle\Tools;

use Tools;

/**
 * Class PageNavigation
 *
 * @package Onyx\AppBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class PageNavigation extends \app\Tools\Page
{

    /** @var int $pageStep */
    protected $pageStep = 6;
    /** @var string $pageEntity */
    protected $pageEntity = 'navigation';
}
