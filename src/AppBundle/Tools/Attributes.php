<?php

namespace src\AppBundle\Tools;

use app\Core\Form;
use app\Facade\Tools;

class Attributes extends Tools
{

    /**
     * @param bool $initialValue
     *
     * @return bool
     */
    public function getBooleanOppositeValue($initialValue)
    {
        if (empty($initialValue) || $initialValue == 0 || $initialValue === false){
            return true;
        }

        return false;
    }

    /**
     * @param Form $form
     * @param array $checkboxIndexes
     * @param int $defaultValue
     *
     * @return Form
     */
    public function getFormUpdatedWithCheckboxValues(Form $form, array $checkboxIndexes, $defaultValue = 0)
    {
        /** @var array $formData */
        $formData = $form->getData();

        foreach ($checkboxIndexes as $checkboxIndex) {
            if (!isset($formData[$checkboxIndex])) {
                $form->setFieldValue($checkboxIndex, $defaultValue);
            }
        }

        return $form;
    }
}
