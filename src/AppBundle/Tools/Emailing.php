<?php

namespace src\AppBundle\Tools;

use app\Core\ClassManager;
use app\Core\TemplateManager;
use \PHPMailer;
use ErrorManager;

/**
 * Class Emailing
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Emailing extends \app\Facade\Tools
{

    /** @var string DEFAULT_ELEXYR_SENDER_ADDRESS */
    const DEFAULT_ELEXYR_SENDER_ADDRESS = 'contact@elexyr.io';
    /** @var string DEFAULT_ELEXYR_SENDER_ADDRESS_NOREPLY */
    const DEFAULT_ELEXYR_SENDER_ADDRESS_NOREPLY = 'noreply@elexyr.io';
    /** @var string DEFAULT_ELEXYR_SENDER_NAME */
    const DEFAULT_ELEXYR_SENDER_NAME = 'Elexyr Team';
    /** @var string EMAIL_HEADER_IMAGE_PATH */
    const EMAIL_HEADER_IMAGE_PATH = 'http://www.elexyr.io/web/bundle/AppBundle/img/global/elexyr.svg';

    /**
     * Send an email according to defined params
     * @param string $senderAddress
     * @param string $senderName
     * @param array $recipients
     * @param string $subject
     * @param string $contentHtml
     * @param string $charset
     *
     * @return bool
     */
    public function send($senderAddress, $senderName, array $recipients, $subject, $contentHtml, $charset = 'utf-8')
    {
        /** @var PHPMailer $mail */
        $mail = new PHPMailer();
        // set mail charset
        $mail->CharSet = $charset;
        // allow html content
        $mail->isHTML(true);
        // add sender information
        $mail->setFrom($senderAddress, $senderName);
        // add recipients information
        foreach ($recipients as $recipientName => $recipientAddress) {
            $mail->addAddress($recipientAddress, $recipientName);
        }
        // add email subject
        $mail->Subject = $subject;
        // add email content
        $mail->Body = $contentHtml;

        // log error in case of failure
        if(!$mail->send()) {
            ErrorManager::log('Mailer error : ' . $mail->ErrorInfo);

            return false;
        }

        return true;
    }

    /**
     * Get email template from code and parse params
     * @param string $templateCode
     * @param array $templateParams
     *
     * @return string
     */
    public function getTemplate($templateCode, $templateParams)
    {
        /** @var array $parts */
        $parts = explode(ClassManager::MODULE_IDENTIFIER_DELIMITER, $templateCode);
        // if main template bundle is defined
        if (count($parts) === 2) {
            $bundleIdentifier = $parts[0];
            $templateCode = $parts[1];
        }
        // if bundle is not set, use current bundle
        if (empty($bundleIdentifier)) {
            /** @var string $bundleIdentifier */
            $bundleIdentifier = Session::getData_('current_bundle');
        }
        if (empty($bundleIdentifier)) {
            ErrorManager::throwException('No current bundle find to render ' . $templateCode . ' email template');
        }

        /** @var string $templatePath */
        $templatePath = TemplateManager::getEmailTemplatePath($templateCode, $bundleIdentifier);
        // if template file is not found
        if (!file_exists($templatePath)) {
            return '';
        }

        /** @var string $emailContent */
        $emailContent =  file_get_contents($templatePath);
        // parse email template params
        foreach($templateParams as $key => $value)
        {
            $emailContent = str_replace('{{ '.$key.' }}', $value, $emailContent);
        }

        return $emailContent;
    }
}
