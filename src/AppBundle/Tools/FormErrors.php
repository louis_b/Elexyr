<?php

namespace src\AppBundle\Tools;

use app\Core\Form;

class FormErrors extends \app\Tools\FormErrors
{
    /** @var string FAILURE_EMPTY */
    const FAILURE_EMPTY = 'empty';
    /** @var string FAILURE_MAXLENGTH */
    const FAILURE_MAXLENGTH = 'maxLength';
    /** @var string FAILURE_MINLENGTH */
    const FAILURE_MINLENGTH = 'minLength';
    /** @var string FAILURE_MIN */
    const FAILURE_MIN = 'min';
    /** @var string FAILURE_MAX */
    const FAILURE_MAX = 'max';
    /** @var string FAILURE_MAXSIZE */
    const FAILURE_MAXSIZE = 'maxSize';
    /** @var string FAILURE_EXTENSION */
    const FAILURE_EXTENSION = 'extension';
    /** @var string FAILURE_MIMETYPE */
    const FAILURE_MIMETYPE = 'mimeType';
    /** @var string FAILURE_DATEFORMAT */
    const FAILURE_DATEFORMAT = 'dateFormat';
    /** @var string FAILURE_DATEMAX */
    const FAILURE_DATEMAX = 'dateMax';
    /** @var string FAILURE_DATEMIN */
    const FAILURE_DATEMIN = 'dateMin';
    /** @var string FAILURE_EMAIL */
    const FAILURE_EMAIL = 'emailFormat';
    /** @var string FAILURE_INT */
    const FAILURE_INT = 'intFormat';
    /** @var string FAILURE_MINCAPS */
    const FAILURE_MINCAPS = 'minCaps';
    /** @var string FAILURE_MINNONALPHA */
    const FAILURE_MINNONALPHA = 'minNonAlpha';
    /** @var string FAILURE_UNIQUE */
    const FAILURE_UNIQUE = 'uniqueValue';

    /**
     * Format Form failures in human readable messages
     * @param Form $form
     *
     * @return array
     */
    public function getFormattedFormErrors(Form $form)
    {
        /** @var array $failures */
        $failures = $form->getFailures();
        /** @var array $formattedFailures */
        $formattedFailures = [];
        // check each field
        foreach ($failures as $fieldCode => $fieldFailures) {
            /** @var string $fieldName */
            $fieldName = $this->getFormattedFieldCode($fieldCode);
            // check failure for each field
            foreach ($fieldFailures as $failureType => $expectedValue) {
                // empty failure
                if ($failureType === self::FAILURE_EMPTY) {
                    $formattedFailures[] = $this->getEmptyFailureMessage($fieldName);
                    break;
                }
                // email format failure
                if ($failureType === self::FAILURE_EMAIL) {
                    $formattedFailures[] = $this->getEmailFormatFailureMessage($fieldName);
                    break;
                }
                // int format failure
                if ($failureType === self::FAILURE_INT) {
                    $formattedFailures[] = $this->getIntFormatFailureMessage($fieldName);
                    break;
                }
                // date format failure
                if ($failureType === self::FAILURE_DATEFORMAT) {
                    $formattedFailures[] = $this->getDateFormatFailureMessage($fieldName);
                    break;
                }
                // max length failure
                if ($failureType === self::FAILURE_MAXLENGTH) {
                    $formattedFailures[] = $this->getMaxLengthFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // min length failure
                if ($failureType === self::FAILURE_MINLENGTH) {
                    $formattedFailures[] = $this->getMinLengthFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // min failure
                if ($failureType === self::FAILURE_MIN) {
                    $formattedFailures[] = $this->getMinFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // max failure
                if ($failureType === self::FAILURE_MAX) {
                    $formattedFailures[] = $this->getMaxFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // date max failure
                if ($failureType === self::FAILURE_DATEMAX) {
                    $formattedFailures[] = $this->getDateMaxFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // date min failure
                if ($failureType === self::FAILURE_DATEMIN) {
                    $formattedFailures[] = $this->getDateMinFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // min caps failure
                if ($failureType === self::FAILURE_MINCAPS) {
                    $formattedFailures[] = $this->getMinCapsFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // min non alphanumeric failure
                if ($failureType === self::FAILURE_MINNONALPHA) {
                    $formattedFailures[] = $this->getMinNonAlphaFailureMessage($fieldName, $expectedValue);
                    continue;
                }
                // unique failure
                if ($failureType === self::FAILURE_UNIQUE) {
                    $formattedFailures[] = $this->getUniqueValueFailureMessage($fieldName, $expectedValue);
                    continue;
                }

                /** @var array $customFailures */
                $customFailure = $this->customFailureChecks($failureType, $fieldName, $expectedValue);
                if ($customFailure !== false) {
                    $formattedFailures[] = $customFailure;
                    continue;
                }

                // manage file failures
                if (!is_array($expectedValue)) {
                    continue;
                }
                /** @var string $fileName */
                $fileName = $failureType;
                foreach ($expectedValue as $failureType => $fileFailure) {
                    // file extension failure
                    if ($failureType === self::FAILURE_EXTENSION) {
                        $formattedFailures[] = $this->getExtensionsFailureMessage($fieldName, $fileFailure, $fileName);
                        break;
                    }
                    // max size failure
                    if ($failureType === self::FAILURE_MAXSIZE) {
                        $formattedFailures[] = $this->getMaxSizeFailureMessage($fieldName, $fileFailure, $fileName);
                        continue;
                    }
                    // mime type failure
                    if ($failureType === self::FAILURE_MIMETYPE) {
                        $formattedFailures[] = $this->getMimeTypeFailureMessage($fieldName, $fileName);
                        continue;
                    }
                }
            }
        }

        return $formattedFailures;
    }

    /**
     * Function to override for custom checks
     * @param string $failureType
     * @param string $fieldName
     * @param mixed $expectedValue
     *
     * @return bool|string
     */
    public function customFailureChecks($failureType, $fieldName, $expectedValue)
    {
        return false;
    }

    /**
     * Format field code to a displayable content
     * @param string $fieldCode
     *
     * @return string
     */
    public function getFormattedFieldCode($fieldCode)
    {
        if (strpos($fieldCode, '_') === false) {
            return $fieldCode;
        }
        /** @var string $fieldCode */
        $fieldCode = str_replace('_', ' ', $fieldCode);

        return $fieldCode;
    }

    /**
     * Return failure message formatted for empty failure
     * @param string $fieldCode
     *
     * @return string
     */
    public function getEmptyFailureMessage($fieldCode)
    {
        return ucfirst($fieldCode) . ' must be filled in. It cannot be empty.';
    }

    /**
     * Return failure message formatted for max length failure
     * @param string $fieldCode
     * @param int $expectedMaxLength
     *
     * @return string
     */
    public function getMaxLengthFailureMessage($fieldCode, $expectedMaxLength)
    {
        /** @var bool $isPlural */
        $isPlural = true;
        if ($expectedMaxLength <= 1) {
            $isPlural = false;
        }
        /** @var string $message */
        $message = ucfirst($fieldCode) . ' must contain a maximum of ' . $expectedMaxLength;
        if ($isPlural === true) {
            $message .= ' characters.';
        }
        if ($isPlural === false) {
            $message .= ' character.';
        }

        return $message;
    }

    /**
     * Return failure message formatted for min length failure
     * @param string $fieldCode
     * @param int $expectedMinLength
     *
     * @return string
     */
    public function getMinLengthFailureMessage($fieldCode, $expectedMinLength)
    {
        /** @var bool $isPlural */
        $isPlural = true;
        if ($expectedMinLength <= 1) {
            $isPlural = false;
        }
        /** @var string $message */
        $message = ucfirst($fieldCode) . ' must contain a minimum of ' . $expectedMinLength;
        if ($isPlural === true) {
            $message .= ' characters.';
        }
        if ($isPlural === false) {
            $message .= ' character.';
        }

        return $message;
    }

    /**
     * Return failure message formatted for min number failure
     * @param string $fieldCode
     * @param int $expectedMin
     *
     * @return string
     */
    public function getMinFailureMessage($fieldCode, $expectedMin)
    {
        return ucfirst($fieldCode) . ' must be greater than ' . $expectedMin;
    }

    /**
     * Return failure message formatted for max number failure
     * @param string $fieldCode
     * @param int $expectedMax
     *
     * @return string
     */
    public function getMaxFailureMessage($fieldCode, $expectedMax)
    {
        return ucfirst($fieldCode) . ' must be less than ' . $expectedMax;
    }

    /**
     * Return failure message formatted for max size failure
     * @param string $fieldCode
     * @param string $expectedMax
     * @param string $filename
     *
     * @return string
     */
    public function getMaxSizeFailureMessage($fieldCode, $expectedMax, $filename)
    {
        return ucfirst($fieldCode) . ' file ('. $filename .') must have a maximum size of ' . $expectedMax;
    }

    /**
     * Return failure message formatted for file extension failure
     * @param string $fieldCode
     * @param array $expectedExtensions
     * @param string $filename
     *
     * @return string
     */
    public function getExtensionsFailureMessage($fieldCode, array $expectedExtensions, $filename)
    {
        /** @var bool $isPlural */
        $isPlural = true;
        if (count($expectedExtensions) === 1) {
            $isPlural = false;
        }
        if (count($expectedExtensions) < 0) {
            return ucfirst($fieldCode) . ' file (' . $filename . ') extension is not valid';
        }
        /** @var string $message */
        $message = ucfirst($fieldCode) . ' file (' . $filename . ') must be';
        if ($isPlural) {
            $message .= ' of one of the following types : ' . implode(', ', $expectedExtensions);
        } else {
            $message .= ' a ' . $expectedExtensions[0] . ' file';
        }

        return $message;
    }

    /**
     * Return failure message formatted for mime type failure
     * @param string $fieldCode
     * @param string $filename
     *
     * @return string
     */
    public function getMimeTypeFailureMessage($fieldCode, $filename)
    {
        return ucfirst($fieldCode) . ' file ('. $filename .') is not a valid file.';
    }

    /**
     * Return failure message formatted for email format failure
     * @param string $fieldCode
     *
     * @return string
     */
    public function getEmailFormatFailureMessage($fieldCode)
    {
        return ucfirst($fieldCode) . ' is not a valid email address.';
    }

    /**
     * Return failure message formatted for int format failure
     * @param string $fieldCode
     *
     * @return string
     */
    public function getIntFormatFailureMessage($fieldCode)
    {
        return ucfirst($fieldCode) . ' is not a valid number.';
    }

    /**
     * Return failure message formatted for date format failure
     * @param string $fieldCode
     *
     * @return string
     */
    public function getDateFormatFailureMessage($fieldCode)
    {
        return ucfirst($fieldCode) . ' is not a valid date format.';
    }

    /**
     * Return failure message formatted for min date failure
     * @param string $fieldCode
     * @param string $expectedMin
     *
     * @return string
     */
    public function getDateMinFailureMessage($fieldCode, $expectedMin)
    {
        return ucfirst($fieldCode) . ' date must be greater than ' . $expectedMin;
    }

    /**
     * Return failure message formatted for max date failure
     * @param string $fieldCode
     * @param string $expectedMax
     *
     * @return string
     */
    public function getDateMaxFailureMessage($fieldCode, $expectedMax)
    {
        return ucfirst($fieldCode) . ' date must be less than ' . $expectedMax;
    }

    /**
     * Return failure message formatted for min caps failure
     * @param string $fieldCode
     * @param int $expectedMin
     *
     * @return string
     */
    public function getMinCapsFailureMessage($fieldCode, $expectedMin)
    {
        /** @var bool $isPlural */
        $isPlural = true;
        if ($expectedMin <= 1) {
            $isPlural = false;
        }
        /** @var string $message */
        $message = ucfirst($fieldCode) . ' must contain at least ' . $expectedMin;
        if ($isPlural === true) {
            $message .= ' capital letters.';
        }
        if ($isPlural === false) {
            $message .= ' capital letter.';
        }

        return $message;
    }

    /**
     * Return failure message formatted for min non alphanumeric failure
     * @param string $fieldCode
     * @param int $expectedMin
     *
     * @return string
     */
    public function getMinNonAlphaFailureMessage($fieldCode, $expectedMin)
    {
        /** @var bool $isPlural */
        $isPlural = true;
        if ($expectedMin <= 1) {
            $isPlural = false;
        }
        /** @var string $message */
        $message = ucfirst($fieldCode) . ' must contain at least ';
        if ($isPlural === true) {
            $message .= $expectedMin .' non alphanumeric characters.';
        }
        if ($isPlural === false) {
            $message .= 'a non alphanumeric character.';
        }

        return $message;
    }

    /**
     * Return failure message formatted for unique value failure
     * @param string $fieldCode
     * @param mixed $submittedValue
     *
     * @return string
     */
    public function getUniqueValueFailureMessage($fieldCode, $submittedValue)
    {
        /** @var string $message */
        $message = ucfirst($fieldCode) . ' must be unique. "' . $submittedValue . '" already exists.';

        return $message;
    }
}
