<?php

namespace src\AppBundle\Tools;

/**
 * Class Token
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Token extends \app\Facade\Tools
{

    /**
     * Get a unique php token
     * @return string
     */
    public function generateUniqueToken()
    {
        return md5(uniqid(rand(), true));
    }
}
