<?php

namespace src\AppBundle\Tools;

use Tools;

/**
 * Class Page
 *
 * @package Onyx\AppBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Page extends \app\Tools\Page
{

    /** @var int $pageStep */
    protected $pageStep = 24;

    /**
     * Get next page for mixed media listing
     * @param $pageNumber
     *
     * @return bool|int
     */
    public function getMixedNextPage($pageNumber)
    {
        /** @var array $mediaTypes */
        $mediaTypes = Media::AVAILABLE_MEDIA_TYPES;
        /** @var Media $mediaHelper */
        $mediaHelper = Tools::get('app/media');
        /** @var bool $nextPageExists */
        $nextPageExists = false;
        // page step for image
        $this->setPageStep(12);

        foreach ($mediaTypes as $mediaType) {
            /** @var string $mediaIdentifier */
            $mediaIdentifier = $mediaHelper->getEntityIdentifierByType($mediaType);
            // set entity type
            $this->setPageEntity($mediaIdentifier);
            /** @var bool|int $nextPage */
            $nextPage = $this->getNextPage($pageNumber);

            if ($nextPage !== false) {
                return $nextPage;
            }
            // page step for medias other than images
            $this->setPageStep(6);
        }

        return $nextPageExists;
    }
}
