<?php

namespace src\AppBundle\Tools;

use app\Facade\Tools;
use app\Orm\Query\Select;
use Collection;
use Repository;

class Media extends Tools
{

    /** @var string MEDIA_ENTITY_PREFIX */
    const MEDIA_ENTITY_PREFIX = 'Media';
    /** @var array AVAILABLE_MEDIA_TYPES */
    const AVAILABLE_MEDIA_TYPES = ['image', 'pictogram', 'pdf', 'video'];
    /** @var string MEDIA_ROUTE_EDIT_PREFIX */
    const MEDIA_ROUTE_EDIT_PREFIX = 'media';
    /** @var string MEDIA_ROUTE_EDIT_SUFFIX */
    const MEDIA_ROUTE_EDIT_SUFFIX = 'edit';
    /** @var string MEDIA_ROUTE_SEPARATOR */
    const MEDIA_ROUTE_SEPARATOR = '_';

    /**
     * Get media entity identifier name from type
     * @param string $type
     *
     * @return string
     */
    public function getEntityIdentifierByType($type)
    {
        /** @var string $identifier */
        $identifier = self::MEDIA_ENTITY_PREFIX . ucfirst($type);

        return $identifier;
    }

    /**
     * Check if media type is an available one
     * @param $type
     * @return bool
     */
    public function isValidType($type)
    {
        if (in_array($type, self::AVAILABLE_MEDIA_TYPES)) {
            return true;
        }

        return false;
    }

    /**
     * Get edit route code associated to a specific media type
     * @param string $type
     *
     * @return string
     */
    public function getEditRouteCode($type)
    {
        /** @var string $routeCode */
        $routeCode = self::MEDIA_ROUTE_EDIT_PREFIX;
        $routeCode .= self::MEDIA_ROUTE_SEPARATOR;
        $routeCode .= $type;
        $routeCode .= self::MEDIA_ROUTE_SEPARATOR;
        $routeCode .= self::MEDIA_ROUTE_EDIT_SUFFIX;

        return $routeCode;
    }

    /**
     * Get collection with all media types
     * @param int $pageNumber
     *
     * @return array
     */
    public function getMixedCollection($pageNumber)
    {
        /** @var array $mediaTypes */
        $mediaTypes = self::AVAILABLE_MEDIA_TYPES;
        /** @var Page $pageHelper */
        $pageHelper = Tools::get('app/page')->setPageStep(12);

        /** @var array $allMedias */
        $allMedias = [];
        foreach ($mediaTypes as $mediaType) {
            /** @var string $mediaIdentifier */
            $mediaIdentifier = $this->getEntityIdentifierByType($mediaType);
            // set entity type
            $pageHelper->setPageEntity($mediaIdentifier);

            /** @var \app\Orm\Collection $mediaCollection */
            $mediaCollection = Collection::get($mediaIdentifier);
            $mediaCollection = $pageHelper->addLimitToCollection($mediaCollection, $pageNumber);
            $mediaCollection = $mediaCollection->sortBy('updated_at', Select::SORT_ORDER_DESC)->all();

            // get only 6 items except for images (12 for it)
            $pageHelper->setPageStep(6);

            // add new medias to collection
            $allMedias = array_merge($allMedias, $mediaCollection);
        }
        $allMedias = $this->sortAllMediaCollection($allMedias);

        return $allMedias;
    }

    /**
     * Sort media by update date
     * @param array $allMedias
     *
     * @return array
     */
    public function sortAllMediaCollection(array $allMedias)
    {
        uasort($allMedias, function ($value1, $value2) {
            /** @var string $value1 */
            $value1 = $value1->getUpdatedAt();
            /** @var string $value2 */
            $value2 = $value2->getUpdatedAt();

            if ($value1 == $value2) {
                return 0;
            }

            return ($value1 > $value2) ? -1 : 1;
        });

        return $allMedias;
    }

    /**
     * Get media type by entity identifier
     * @param string $mediaType
     *
     * @return string
     */
    public function getMediaTypeByEntityIdentifier($mediaType)
    {
        /** @var string $mediaEntityPrefix */
        $mediaEntityPrefix = self::MEDIA_ENTITY_PREFIX;

        return str_replace($mediaEntityPrefix, '', $mediaType);
    }

    /**
     * Delete media by ids
     * @param string $mediaType
     * @param array $idsToDelete
     */
    public function massMediaDelete($mediaType, array $idsToDelete)
    {
        /** @var string $mediaIdentifier */
        $mediaIdentifier = $this->getEntityIdentifierByType($mediaType);

        Repository::get($mediaIdentifier)->massDelete($idsToDelete);
    }
}
