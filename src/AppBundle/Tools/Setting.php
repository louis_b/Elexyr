<?php

namespace src\AppBundle\Tools;

use app\Core\BundleManager;
use app\Core\FileManager;
use Parser;

/**
 * Class Setting
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Setting extends \app\Facade\Tools
{

    /** @var string SETTINGS_CONFIGURATION_FILE */
    const SETTINGS_CONFIGURATION_FILE = "Configs/settings.yml";

    /**
     * Get settings from yml configuration file
     * @return array
     */
    public function getSettings()
    {
        /** @var string $settingsFilePath */
        $settingsFilePath = BundleManager::getBundlePath('app');
        $settingsFilePath .= str_replace(FileManager::CONFIG_DS, DS,self::SETTINGS_CONFIGURATION_FILE);
        if (!file_exists($settingsFilePath)) {
            ErrorManager::log('Settings file configuration not find in ' . $settingsFilePath);

            return [];
        }
        /** @var array $settingsConfiguration */
        $settingsConfiguration = Parser::get('yml')->getFileData($settingsFilePath);

        return $settingsConfiguration;
    }
}
