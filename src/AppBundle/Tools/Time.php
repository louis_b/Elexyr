<?php

namespace src\AppBundle\Tools;

use OnyxDate;

/**
 * Class Time
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Time extends \app\Facade\Tools
{

    /** @var int DAY_BEGINNING_HOUR */
    const DAY_BEGINNING_HOUR = 8;
    /** @var int NIGHT_BEGINNING_HOUR */
    const NIGHT_BEGINNING_HOUR = 19;

    /**
     * Check if its currently the night
     * @return bool
     */
    public function isNight()
    {
        /** @var \app\Core\OnyxDate $datetime */
        $datetime = OnyxDate::get();
        /** @var int $hours */
        $hours = (int) $datetime->format('H');

        if ($hours < self::DAY_BEGINNING_HOUR || $hours > self::NIGHT_BEGINNING_HOUR) {
            return true;
        }

        return false;
    }
}
