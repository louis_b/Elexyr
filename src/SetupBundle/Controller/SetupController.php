<?php

namespace src\SetupBundle\Controller;

use app\Core\FormManager;
use app\Core\RouteManager;
use Request;
use Template;
use Collection;
use Tools;
use OnyxDate;
use Form;
use Entity;

/**
 * Class SetupController
 *
 * @package Elexyr\AppBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class SetupController
{
    /**
     * first setup step
     * @param array $params
     */
    public function stepOneAction(array $params)
    {
        /** @var bool $isAlreadyInstalled */
        $isAlreadyInstalled = Tools::get('setup/install')->isAlreadyInstalled();
        if ($isAlreadyInstalled === true) {
            RouteManager::redirectToRoute('home', [], 'app');
        }

        Template::get();
    }

    /**
     * second setup step
     * @param array $params
     */
    public function stepTwoAction(array $params)
    {
        /** @var bool $isAlreadyInstalled */
        $isAlreadyInstalled = Tools::get('setup/install')->isAlreadyInstalled();
        if ($isAlreadyInstalled === true) {
            RouteManager::redirectToRoute('home', [], 'app');
        }

        /** @var array $settings */
        $settings = ['website_logo', 'website_name'];
        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Core\Form $form */
        $form = Form::get('setup/websiteInit');
        /** @var \app\Orm\Entity $settingEntity */
        $settingEntity = Entity::get('Setting');
        /** @var \app\Orm\Entity $websiteImageSetting */
        $websiteImageSetting = $settingEntity->loadBy('code', 'website_logo');

        if ($form->isSubmit() === false) {
            // pre fill settings values
            foreach ($settings as $settingCode) {
                /** @var string $settingValue */
                $settingValue = $settingEntity->loadBy('code', $settingCode)->getValue();
                if (!empty($settingValue)) {
                    // add value to form
                    $form->setFieldValue($settingCode, $settingValue);
                }
            }
        }
        // set website image as non required if already exists
        if ($form->isSubmit() && !empty($websiteImageSetting->getValue())) {
            /** @var array $formFields */
            $formFields = $form->getFields();
            $formFields['website_logo']['required'] = false;
            $form->setFields($formFields);
        }
        // submit form
        $form->submit();
        // manage empty form
        if (!isset($params['website_name'])) {
            $form->setIsValid(false);
        }

        if ($form->isSubmit() === true && $form->getIsValid() === true) {
            // set value to configuration
            foreach ($settings as $settingCode) {
                $settingEntity->loadBy('code', $settingCode)
                    ->setCode($settingCode)
                    ->setValue($form->getFieldValue($settingCode))
                    ->setUpdatedAt($date)
                    ->save();
            }
            // redirect on success
            RouteManager::redirectToRoute('step_three');
        }
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors,
            'websiteImageSetting' => $settingEntity->loadBy('code', 'website_logo')
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * third setup step
     * @param array $params
     */
    public function stepThreeAction(array $params)
    {
        /** @var bool $isAlreadyInstalled */
        $isAlreadyInstalled = Tools::get('setup/install')->isAlreadyInstalled();
        if ($isAlreadyInstalled === true) {
            RouteManager::redirectToRoute('home', [], 'app');
        }

        /** @var int $userId */
        $userId = Tools::get('user/user')->getCurrentUserId();
        if (!empty($userId)) {
            RouteManager::redirectToRoute('step_three_exists');
        }
        // if there is already existing users, superadmin cannot be created
        /** @var int $userNumber */
        $userNumber = Collection::get('User')->getSize();
        if ($userNumber > 0) {
            RouteManager::redirectToRoute('step_four');
        }

        /** @var \app\Core\OnyxDate $date */
        $date = OnyxDate::get()->toString();
        /** @var \app\Core\Form $form */
        $form = Form::get('setup/registerAdministrator');

        if ($form->isSubmit() === true && isset($params['user_password'])) {
            /** @var string $password */
            $password = Tools::get('user/password')->getHashedPassword($params['user_password']);

            $form->setFieldValue('password', $password);
            $form->setFieldValue('role', 'superadmin');
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        $form->submit();

        // manage user image creation
        if ($form->getIsValid() === true && FormManager::isEmptyFile($params['user_image']) === false) {
            /** @var \app\Orm\Entity $createdUser */
            $createdUser = Collection::get('User')->getLastEntity();
            // create new image
            Entity::get('UserImage')
                ->setUserId($createdUser->getEntityId())
                ->setPath($form->getFieldValue('user_image'))
                ->setIsMainImage(1)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->save();
        }
        if ($form->getIsValid() === true) {
            // connect user after password reset
            Tools::get('user/authentication')->connectUser($form->getFieldValue('email'));
            RouteManager::redirectToRoute('step_four');
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'formErrors' => $formErrors
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Step three action if user already exists
     * @param array $params
     */
    public function stepThreeExistsAction(array $params)
    {
        /** @var bool $isAlreadyInstalled */
        $isAlreadyInstalled = Tools::get('setup/install')->isAlreadyInstalled();
        if ($isAlreadyInstalled === true) {
            RouteManager::redirectToRoute('home', [], 'app');
        }

        /** @var int $userId */
        $userId = Tools::get('user/user')->getCurrentUserId();
        if (empty($userId)) {
            RouteManager::redirectToRoute('step_three');
        }

        /** @var \app\Orm\Entity $currentUser */
        $currentUser = Entity::get('User')->loadById($userId);
        /** @var array $viewParams */
        $viewParams = [
            'currentUser' => $currentUser,
            'userMainImagePath' => Tools::get('user/image')->getUserMainImagePath($userId)
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * fourth setup step
     * @param array $params
     */
    public function stepFourAction(array $params)
    {
        /** @var bool $isAlreadyInstalled */
        $isAlreadyInstalled = Tools::get('setup/install')->isAlreadyInstalled();
        if ($isAlreadyInstalled === true) {
            RouteManager::redirectToRoute('home', [], 'app');
        }

        /** @var int $userId */
        $userId = Tools::get('user/user')->getCurrentUserId();
        if (empty($userId)) {
            RouteManager::redirectToRoute('step_one');
        }
        // end installation process
        Tools::get('setup/install')->finishInstall();

        Template::get();
    }
}