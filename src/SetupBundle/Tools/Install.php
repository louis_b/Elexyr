<?php

namespace src\SetupBundle\Tools;

use app\Core\Onyx;

/**
 * Class Install
 *
 * @package Elexyr\SetupBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Install extends \app\Facade\Tools
{

    /** @var string ELEXYR_INSTALLATION_FILE */
    const ELEXYR_INSTALLATION_FILE = 'install.elexyr';
    /** @var array INSTALL_ROUTES_CODES */
    const INSTALL_ROUTES_CODES = ['step_one', 'step_two', 'step_three', 'step_three_exists', 'step_four'];

    /**
     * Check if Elexyr has been installed
     * @return bool
     */
    public function isAlreadyInstalled()
    {
        /** @var string $installFilePath */
        $installFilePath = $this->getInstallFilePath();

        if (file_exists($installFilePath)) {
            return false;
        }

        return true;
    }

    /**
     * Get installation flag file path
     * @return string
     */
    public function getInstallFilePath()
    {
        /** @var string $filePath */
        $filePath = Onyx::getProjectRoot();
        $filePath .= self::ELEXYR_INSTALLATION_FILE;

        return $filePath;
    }

    /**
     * Check if specific route belongs to install process
     * @param string $routeCode
     *
     * @return bool
     */
    public function isInstallationRoute($routeCode)
    {
        /** @var array $installRoutes */
        $installRoutes = self::INSTALL_ROUTES_CODES;
        /** @var bool $isInstall */
        $isInstall = in_array($routeCode, $installRoutes);

        return $isInstall;
    }

    /**
     * Delete installation flag file to end installation
     */
    public function finishInstall()
    {
        /** @var string $installFilePath */
        $installFilePath = $this->getInstallFilePath();

        if (file_exists($installFilePath)) {
            unlink($installFilePath);
        }
    }
}
