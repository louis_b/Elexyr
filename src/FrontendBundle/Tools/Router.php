<?php

namespace src\FrontendBundle\Tools;

use app\Core\Onyx;
use app\Core\Request;
use app\Facade\Tools;
use Collection;

/**
 * Class Router
 *
 * @package Elexyr\FrontendBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class Router extends Tools
{

    /**
     * Check if current route exists in frontend
     * @param string $uri
     *
     * @return bool
     */
    public function isFrontendRoute($uri)
    {
        /** @var string $uri */
        $uri = $this->manageHomeUri($uri);

        /** @var int $count */
        $count = Collection::get('page')
            ->filterBy('url_key', $uri)
            ->filterBy('is_active', 1)
            ->getSize();
        if ($count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get page associated to current url
     * @param string $uri
     *
     * @return \app\Orm\Entity
     */
    public function getRouterPageObject($uri)
    {
        /** @var string $uri */
        $uri = $this->manageHomeUri($uri);

        /** @var \app\Orm\Entity $count */
        $page = Collection::get('page')
            ->filterBy('url_key', $uri)
            ->filterBy('is_active', 1)
            ->first();

        return $page;
    }

    /**
     * Manage homepage uri value
     * @param string $uri
     *
     * @return string
     */
    public function manageHomeUri($uri)
    {
        if (empty($uri)) {
            $uri = '/';
        }

        return $uri;
    }

    /**
     * Get frontend url using url key
     * @param string $urlKey
     *
     * @return string
     */
    public function getFrontendUrl($urlKey = '/')
    {
        /** @var string $urlPath */
        $urlPath = Onyx::getUrl(trim($urlKey, Request::URL_DELIMITER));

        return $urlPath;
    }
}
