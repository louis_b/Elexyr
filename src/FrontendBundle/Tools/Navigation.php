<?php

namespace src\FrontendBundle\Tools;

use app\Facade\Tools;
use Repository;
use Entity;
use Collection;

class Navigation extends Tools
{
    /**
     * get all data of the main navigation
     * @return array
     */
    public function getMainNavigationData()
    {
        $navigationData = Collection::get('navigation')
            ->filterBy('is_default', 1, '=')
            ->all();

        $navData = [];

        foreach ($navigationData as $key => $navigation){
            $navData['navigation' . $key]['data'] = $navigation;
            $navData['navigation' . $key]['items'] = Collection::get('navigationItem')
                ->filterBy('navigation_id', $navigation->getEntityId(), '=')
                ->sortBy('position', 'ASC')
                ->all();
        }

        return $navData;
    }
}