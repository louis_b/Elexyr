<?php

namespace src\FrontendBundle\Controller;

use Session;
use Template;
use Tools;
use Entity;
use Collection;

/**
 * Class FrontController
 *
 * @package Elexyr\FrontendBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class FrontController
{

    /**
     * Action for dynamic frontend page build
     * @param array $params
     */
    public function pageAction(array $params)
    {
        /** @var \app\Orm\Entity $currentPage */
        $currentPage = Session::_getData('current_page');
        /** @var int $currentPageId */
        $currentPageId = $currentPage->getEntityId();

        /** @var array $pageData */
        $pageData = Tools::get('page/data')->getPageData($currentPageId, true);
        /** @var \src\PageBundle\Tools\ContentType $contentTypeHelper */
        $contentTypeHelper = Tools::get('page/contentType');
        /** @var array $contentTypes */
        $contentTypes = $contentTypeHelper->getAllContentTypes();
        /** @var array $contentTypesHeader */
        $contentTypesHeader = $contentTypeHelper->getAllContentTypesHeader();

        /** @var array $navData */
        $navData = Tools::get('frontend/navigation')->getMainNavigationData();

        /** @var string $metaDescriptionContent */
        $metaDescriptionContent = Entity::get('Setting')->loadBy('code', 'meta_description')->getValue();
        /** @var string $websiteLogoSrc */
        $websiteLogoSrc = Entity::get('Setting')->loadBy('code', 'website_logo')->getMediaPath('value');
        /** @var string $websiteName */
        $websiteName = Entity::get('Setting')->loadBy('code', 'website_name')->getValue();
        /** @var \src\FrontendBundle\Tools\Router $routerHelper */
        $routerHelper = Tools::get('frontend/router');
        /** @var \app\Orm\Collection $articlesCollection */
        $articlesCollection = Collection::get('Article')
            ->filterBy('is_active', 1)
            ->sortBy('updated_at', \app\Orm\Query\Select::SORT_ORDER_DESC)
            ->all();

        /** @var array $viewParams */
        $viewParams = [
            'pageDatas' => $pageData,
            'navData' => $navData,
            'currentPage' => $currentPage,
            'currentPageId' => $currentPageId,
            'currentPageName' => $currentPage->getName(),
            'metaDescriptionContent' => $metaDescriptionContent,
            'websiteLogoSrc' => $websiteLogoSrc,
            'websiteName' => $websiteName,
            'contentTypes' => $contentTypes,
            'contentTypesHeader' => $contentTypesHeader,
            'routerHelper' => $routerHelper,
            'homePageUrl' => $routerHelper->getFrontendUrl(),
            'articlesCollection' => $articlesCollection
        ];
        // render view
        Template::get('front')->Template('main')->setParams($viewParams);
    }
}
