<?php

namespace plugins\BlogBundle\Tools;

/**
 * Class Article
 *
 * @package Onyx\BlogBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Article extends \app\Facade\Tools
{

    /** @var string ARTICLE_MASS_ACTION_PREFIX */
    const ARTICLE_MASS_ACTION_PREFIX = 'article-mass';
}
