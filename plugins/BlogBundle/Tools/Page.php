<?php

namespace plugins\BlogBundle\Tools;

/**
 * Class Page
 *
 * @package Onyx\BlogBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Page extends \app\Tools\Page
{

    /** @var int $pageStep */
    protected $pageStep = 8;
    /** @var string $pageEntity */
    protected $pageEntity = 'article';
}
