<?php

namespace plugins\BlogBundle\Tools;

use Tools;
use Collection;

/**
 * Class Router
 *
 * @package Onyx\BlogBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class Router extends \app\Facade\Tools
{

    /**
     * Check if current route exists for blog article in frontend
     * @param string $uri
     *
     * @return bool
     */
    public function isFrontendArticleRoute($uri)
    {
        /** @var string $uri */
        $uri = Tools::get('frontend/router')->manageHomeUri($uri);

        /** @var int $count */
        $count = Collection::get('Article')
            ->filterBy('url_key', $uri)
            ->filterBy('is_active', 1)
            ->getSize();
        if ($count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get article associated to current url
     * @param string $uri
     *
     * @return \app\Orm\Entity
     */
    public function getRouterArticleObject($uri)
    {
        /** @var string $uri */
        $uri = Tools::get('frontend/router')->manageHomeUri($uri);

        /** @var \app\Orm\Entity $count */
        $article = Collection::get('Article')
            ->filterBy('url_key', $uri)
            ->filterBy('is_active', 1)
            ->first();

        return $article;
    }
}
