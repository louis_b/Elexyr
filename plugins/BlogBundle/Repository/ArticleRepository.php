<?php

namespace plugins\BlogBundle\Repository;

/**
 * Class ArticleRepository
 *
 * @package Onyx\BlogBundle
 * @copyright 2017 - Onyx Framework - All rights reserved
 * @author Yoann Vié
 */
class ArticleRepository extends \app\Orm\Repository
{

    /**
     * Mass delete method by ids
     * @param array $idsToDelete
     */
    public function massDelete(array $idsToDelete)
    {

        foreach ($idsToDelete as $id) {
            $this->delete()->where([
                ['entity_id', '=', $id]
            ]);
        }
    }
}
