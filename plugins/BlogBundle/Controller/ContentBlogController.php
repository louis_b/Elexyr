<?php

namespace plugins\BlogBundle\Controller;

use app\Core\RouteManager;
use Template;
use Entity;
use Tools;
use OnyxDate;
use Form;
use Collection;

/**
 * Class ContentBlogController
 *
 * @package Elexyr\BlogBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class ContentBlogController
{

    /** @var string CONTENT_TYPE_BLOG */
    const CONTENT_TYPE_BLOG = 'ContentBlog';

    /**
     * Create content blog
     * @param array $params
     */
    public function addAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['id'];
        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\Form $form */
        $form = Form::get('blog/ContentBlogCreate');
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        /** create form */
        if ($form->isSubmit()) {
            $form->setFieldValue('content_id', $currentContentId);
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();

        /* ------------------
           DELETE EXISTING CONTENT TYPE ASSOCIATED TO CURRENT CONTENT
        */
        /** @var \app\Orm\Entity $currentContent */
        $currentContent = Entity::get('Content')->loadById($currentContentId);
        /** @var null|\app\Orm\Entity $existingContentType */
        $existingContentType = null;
        if ($form->getIsValid() === true && !empty($currentContent->getContentTypeId()) && !empty($currentContent->getContentType())) {
            $existingContentType = Entity::get($currentContent->getContentType())->loadById($currentContent->getContentTypeId());
        }
        if ($existingContentType instanceof \app\Orm\Entity && $existingContentType->getEntityId()) {
            $existingContentType->delete();
        }

        /* ------------------
           MANAGE CONTENT DATA IN GLOBAL CONTENT OBJECT
        */
        if ($form->getIsValid() === true) {
            /** @var \app\Orm\Entity $createdContentBlog */
            $createdContentBlog = Collection::get(self::CONTENT_TYPE_BLOG)->getLastEntity();

            $currentContent->setContentTypeId($createdContentBlog->getEntityId());
            $currentContent->setContentType(self::CONTENT_TYPE_BLOG);
            $currentContent->setUpdatedAt($date);
            $currentContent->save();

            // redirect on success
            RouteManager::redirectToRoute('content_add', ['page_id' => $currentPageId, 'id' => $currentContentId], 'page');
        }

        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'currentPageItem' => $currentPageItem,
            'currentContentId' => $currentContentId,
            'formErrors' => $formErrors,
            'formParams' => [
                'page_id' => $currentPageItem->getEntityId(),
                'id' => $currentContentId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit content blog action
     * @param array $params
     */
    public function editAction(array $params)
    {
        /** @var int $currentPageId */
        $currentPageId = $params['page_id'];
        /** @var int $currentContentId */
        $currentContentId = $params['content_id'];
        /** @var int $currentContentBlogId */
        $currentContentBlogId = $params['id'];
        /** @var \app\Orm\Entity $currentPageItem */
        $currentPageItem = Entity::get('Page')->loadById($currentPageId);

        /** @var \app\Core\Form $form */
        $form = Form::get('blog/ContentBlogEdit');
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        $form->setEntityId($currentContentBlogId);
        /** create form */
        if ($form->isSubmit()) {
            $form->setFieldValue('content_id', $currentContentId);
            $form->setFieldValue('updated_at', $date);
        }
        // submit form
        $form->submit();
        // manage form failures
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var string $urlKey */
        $urlKey = $currentPageItem->getUrlKey();
        /** @var array $viewParams */
        $viewParams = [
            'form' => $form,
            'currentPageItem' => $currentPageItem,
            'currentContentId' => $currentContentId,
            'formErrors' => $formErrors,
            'formParams' => [
                'page_id' => $currentPageItem->getEntityId(),
                'content_id' => $currentContentId,
                'id' => $currentContentBlogId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($urlKey)
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }
}
