<?php

namespace plugins\BlogBundle\Controller;

use Request;
use Template;
use Session;
use Tools;
use Entity;

/**
 * Class FrontendController
 *
 * @package Elexyr\BlogBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class FrontendController
{

    /**
     * Action for frontend articles pages
     * @param array $params
     */
    public function articleAction(array $params)
    {
        /** @var \app\Orm\Entity $currentArticle */
        $currentArticle = Session::_getData('current_article');
        $currentArticle = $currentArticle->loadById($currentArticle->getEntityId());
        $articleContent = Entity::get('article')->loadById($currentArticle->getEntityId());

        $navData = Tools::get('frontend/navigation')->getMainNavigationData();

        $websiteLogoSrc = Entity::get('Setting')->loadBy('code', 'website_logo')->getMediaPath('value');
        $websiteName = Entity::get('Setting')->loadBy('code', 'website_name')->getValue();

        $viewParams = [
            'currentArticle' => $currentArticle,
            'navData' => $navData,
            'websiteLogoSrc' => $websiteLogoSrc,
            'websiteName' => $websiteName,
            'articleContent' => $articleContent,
            'routerHelper' => Tools::get('frontend/router')
        ];

        Template::get('front')->Template('single')->setParams($viewParams);
    }
}
