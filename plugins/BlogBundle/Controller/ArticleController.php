<?php

namespace plugins\BlogBundle\Controller;

use app\Core\RouteManager;
use app\Orm\Query\Select;
use Entity;
use Request;
use Template;
use Tools;
use Collection;
use Form;
use OnyxDate;
use Repository;

/**
 * Class ArticleController
 *
 * @package Elexyr\BlogBundle
 * @copyright 2017 - Elexyr CMS - All rights reserved
 * @author Yoann Vié | Louis Bertin
 */
class ArticleController
{

    /**
     * List all blog articles action
     * @param array $params
     */
    public function listAction(array $params)
    {
        /** @var \plugins\BlogBundle\Tools\Article $articleHelper */
        $articleHelper = Tools::get('blog/article');
        /** @var string $articleMassActionPrefix */
        $articleMassActionPrefix = $articleHelper::ARTICLE_MASS_ACTION_PREFIX;

        /** @var \plugins\BlogBundle\Tools\Page $paginationHelper */
        $paginationHelper = Tools::get('blog/page');
        /** @var int $pageNumber */
        $pageNumber = 1;
        if (isset($params['page_number']) && $params['page_number'] > 1) {
            $pageNumber = (int) $params['page_number'];
        }

        /** @var array $articles */
        $articles = Collection::get('Article');
        $articles = $paginationHelper->addLimitToCollection($articles, $pageNumber);
        $articles = $articles->sortBy('updated_at', Select::SORT_ORDER_DESC)->all();
        // if collection is empty redirect to first page
        if (count($articles) <= 0 && $pageNumber != 1) {
            RouteManager::redirectToRoute('article_list');
        }

        /** @var int|bool $previousPage */
        $previousPage = $paginationHelper->getPreviousPage($pageNumber);
        /** @var int|bool $nextPage */
        $nextPage = $paginationHelper->getNextPage($pageNumber);

        /** @var array $viewParams */
        $viewParams = [
            'articlesCollection' => $articles,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'articleHelper' => $articleHelper,
            'articleMassActionPrefix' => $articleMassActionPrefix,
            'mediaImageEntity' => Entity::get('MediaImage')
        ];
        Template::get()->setParams($viewParams);
    }

    /**
     * Add article action
     * @param array $params
     */
    public function addAction(array $params)
    {
        /** @var \app\Core\Form $form */
        $form = Form::get('blog/articleCreate');
        /** @var string $date */
        $date = OnyxDate::get()->toString();
        /** @var int $currentUserId */
        $currentUserId = Tools::get('user/user')->getCurrentUserId();

        /** create form */
        if ($form->isSubmit() === true) {
            // manage checkbox values
            /** @var \app\Facade\Tools $form */
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['is_active'], 0);

            $form->setFieldValue('author_id', $currentUserId);
            $form->setFieldValue('created_at', $date);
            $form->setFieldValue('updated_at', $date);
        }
        if ($form->isSubmit() === true && isset($params['url_key'])) {
            /** @var string $urlKey */
            $urlKey = Tools::get('page/urlKey')->generateValidUrlKey($params['url_key']);
            $form->setFieldValue('url_key', $urlKey);
        }
        // submit form
        $form->submit();

        // manage form errors
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'formErrors' => $formErrors,
            'form' => $form,
            'currentMediaImage' => $form->getFieldValue('preview_image'),
            'formParams' => [],
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Edit article action
     * @param array $params
     */
    public function editAction(array $params)
    {
        if (!isset($params['article_id'])) {
            RouteManager::redirectToRoute('article_list');
        }
        /** @var int $articleId */
        $articleId = $params['article_id'];
        /** @var \app\Orm\Entity $articleObject */
        $articleObject = Entity::get('Article')->loadById($articleId);
        /** @var \app\Core\Form $form */
        $form = Form::get('blog/articleEdit')->setEntityObject($articleObject);
        /** @var string $date */
        $date = OnyxDate::get()->toString();

        /** create form */
        if ($form->isSubmit() === true) {
            // manage checkbox values
            /** @var \app\Facade\Tools $form */
            $form = Tools::get('app/attributes')->getFormUpdatedWithCheckboxValues($form, ['is_active'], 0);

            $form->setFieldValue('updated_at', $date);
        }
        if ($form->isSubmit() === true && isset($params['url_key'])) {
            /** @var string $urlKey */
            $urlKey = Tools::get('page/urlKey')->generateValidUrlKey($params['url_key']);
            $form->setFieldValue('url_key', $urlKey);
        }
        // submit form
        $form->submit();

        // manage form errors
        $formErrors = Tools::get('app/formErrors')->getFormattedFormErrors($form);

        /** @var array $viewParams */
        $viewParams = [
            'formErrors' => $formErrors,
            'form' => $form,
            'currentMediaImage' => $form->getFieldValue('preview_image'),
            'formParams' => [
                'article_id' => $articleId
            ],
            'frontendUrl' => Tools::get('frontend/router')->getFrontendUrl($articleObject->getUrlKey())
        ];
        /** render view */
        Template::get()->setParams($viewParams);
    }

    /**
     * Delete an article
     * @param array $params
     */
    public function deleteAction(array $params)
    {
        if (!isset($params['article_id'])) {
            RouteManager::redirectToRoute('article_list');
        }

        /** @var \app\Orm\Entity $articleToDelete */
        $articleToDelete = Entity::get('Article')->loadById($params['article_id']);
        $articleToDelete->delete();

        // redirect to article list page
        RouteManager::redirectToRoute('article_list');
    }

    /**
     * Mass delete for articles
     * @param array $params
     */
    public function massDeleteAction(array $params)
    {
        /** @var array $pageIdsToDelete */
        $pageIdsToDelete = [];

        /** @var \plugins\BlogBundle\Tools\Data $articleHelper */
        $articleHelper = Tools::get('blog/article');
        /** @var string $articleParamPrefix */
        $articleParamPrefix = $articleHelper::ARTICLE_MASS_ACTION_PREFIX;
        // format ids to delete by entities
        foreach ($params as $key => $param) {
            if (strpos($key, $articleParamPrefix) !== false) {
                $pageIdsToDelete[] = str_replace($articleParamPrefix . '-', '', $key);
            }
        }
        // mass delete in database
        Repository::get('Article')->massDelete($pageIdsToDelete);

        // redirect to page home
        RouteManager::redirectToRoute('article_list');
    }

    /**
     * Enable specific article
     * @param array $params
     */
    public function enableAction(array $params)
    {
        if (!isset($params['article_id'])) {
            RouteManager::redirectToRoute('article_list');
        }

        /** Entity */
        Entity::get('Article')->loadById($params['article_id'])->setIsActive(1)->save();

        RouteManager::redirectToRoute('article_list');
    }

    /**
     * Disable specific article
     * @param array $params
     */
    public function disableAction(array $params)
    {
        if (!isset($params['article_id'])) {
            RouteManager::redirectToRoute('article_list');
        }

        /** Entity */
        Entity::get('Article')->loadById($params['article_id'])->setIsActive(0)->save();

        RouteManager::redirectToRoute('article_list');
    }
}
