var Modal = {

    deleteModal: function (deleteButton,modalToOpen) {

        deleteButton = $(deleteButton);
        modalToOpen = $(modalToOpen);

        var modalOverlay = $('.modal-overlay');
        var modalCancel = $('.modal-cancel');

        deleteButton.on('click',function () {
            modalToOpen.fadeIn(300,function(){$(this).focus();});

            modalOverlay.addClass('show').addClass('overlay-delete');

            modalCancel.on('click',function () {
                modalToOpen.fadeOut(100);
                modalOverlay.removeClass('show');
                modalOverlay.removeClass('overlay-delete');
            });

            $(document).mouseup(function (e) {
                if (!modalToOpen.is(e.target) && modalToOpen.has(e.target).length == 0) {
                    modalToOpen.fadeOut(100);
                    modalOverlay.removeClass('show');
                    modalOverlay.removeClass('overlay-delete');
                }
            });
        })
    },

    confirmModal: function (deleteButton,modalDelete) {

        deleteButton = $(deleteButton);
        modalDelete = $(modalDelete);

        var modalOverlay = $('.modal-overlay');
        var modalCancel = $('.modal-cancel');

        deleteButton.on('click',function () {
            modalDelete.fadeIn(300,function(){$(this).focus();});

            modalOverlay.addClass('show').addClass('overlay-confirm');

            modalCancel.on('click',function () {
                modalDelete.fadeOut(100);
                modalOverlay.removeClass('show');
                modalOverlay.removeClass('overlay-confirm');
            });

            $(document).mouseup(function (e) {
                if (!modalDelete.is(e.target) && modalDelete.has(e.target).length == 0) {
                    modalDelete.fadeOut(100);
                    modalOverlay.removeClass('show');
                    modalOverlay.removeClass('overlay-confirm');
                }
            });
        })
    },

    warningModal: function (deleteButton,modalDelete) {

        deleteButton = $(deleteButton);
        modalDelete = $(modalDelete);

        var modalOverlay = $('.modal-overlay');
        var modalCancel = $('.modal-cancel');

        deleteButton.on('click',function () {
            modalDelete.fadeIn(300,function(){$(this).focus();});

            modalOverlay.addClass('show').addClass('overlay-warning');

            modalCancel.on('click',function () {
                modalDelete.fadeOut(100);
                modalOverlay.removeClass('show');
                modalOverlay.removeClass('overlay-warning');
            });

            $(document).mouseup(function (e) {
                if (!modalDelete.is(e.target) && modalDelete.has(e.target).length == 0) {
                    modalDelete.fadeOut(100);
                    modalOverlay.removeClass('show');
                    modalOverlay.removeClass('overlay-warning');
                }
            });
        })
    },

    sendHrefToModal: function (hrefToSend,hrefTarget) {
        hrefToSend = $(hrefToSend);
        hrefTarget = $(hrefTarget);

        hrefToSend.on('click',function (e) {
            e.preventDefault();
            var dataHref =  $(this).closest('a').attr('data-href');
            hrefTarget.closest('a').attr('href', dataHref);
        })
    }

};