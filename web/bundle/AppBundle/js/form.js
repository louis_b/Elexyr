var Form = {

    closeFormError: function (button, divToClose) {

        button = jQuery(button);
        divToClose = jQuery(divToClose);

        button.on('click', function () {
            divToClose.hide();
        });

    }

};