var Input = {
 
    triggerSave: function (button) {
        button = jQuery(button);
        var inputSubmit = button.parent().find(':submit');

        button.on('click',function () {
            inputSubmit.trigger('click');
        })
    },

    toggleContent: function (button, divToOpen, closestParam, savingFeature, messageToShow) {
        button = jQuery(button);
        divToOpen = jQuery(divToOpen);
        closestParam = jQuery(closestParam);

        if (savingFeature !== true) {
            savingFeature = false;
        }

        if ( typeof messageToShow === 'undefined' ){
            messageToShow = '<p class="save-alert">NOT SAVE</p>';
        }

        button.on('click',function (e) {
            e.preventDefault();

            if ($(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).closest(closestParam).find(divToOpen).slideToggle();
                return false;
            }

            $(this).addClass("active");
            $(this).closest(closestParam).find(divToOpen).slideToggle();

            if (savingFeature === true) {
                var divToMonitor = $(this).closest(closestParam).find(divToOpen);
                if (divToMonitor.closest('.navigation-box form').find('.navigation-item .save-alert').length > 0){
                    return false;
                } else {

                    var isActive = false;

                    if (isActive === false){
                        divToMonitor.one('input',function () {
                            if (isActive === false) {
                                divToMonitor.closest('.navigation-box form').find('.navigation-item').append(messageToShow);
                                isActive = true;
                                return false;
                            }
                        });
                    }

                    if (isActive === false){
                        divToMonitor.find('.button-default ').on('click',function () {
                            if (isActive === false) {
                                divToMonitor.closest('.navigation-box form').find('.navigation-item').append(messageToShow);
                                isActive = true;
                                console.log(isActive);

                                return false;
                            }
                        })
                    }

                    if (isActive === false){
                        divToMonitor.find('img.previewInput').on('load',function () {
                            if (isActive === false) {
                                divToMonitor.closest('.navigation-box form').find('.navigation-item').append(messageToShow);
                                isActive = true;
                                console.log(isActive);

                                return false;
                            }
                        })
                    }

                }
            }

        })
    },

    navigationPositionButtons: function (buttons) {
        buttons = jQuery(buttons);

        buttons.on('click',function () {
            $(this).siblings().removeClass('active');
            jQuery(this).addClass('active');
            var buttonValue = jQuery(this).data("value");
            $(this).parent().parent().find('input[type="number"]').attr('value',buttonValue);
        })
    },

    /**
     * select input for Elexyr
     * @param falseSelect
     */
    elexyrSelect: function (falseSelect) {

        falseSelect = jQuery(falseSelect);

        /**
         * create false select when page is loading
         */
        var options = falseSelect.find('select option');
        var optionNumber = options.length;

        if ($(options[0]).attr('disabled')) {
            options = options.slice(1);
            optionNumber -= 1;
        }

        var optionName = [];
        for (var i = 0; i < optionNumber; i++) {
            optionName.push(jQuery(options[i]).text());
        }

        var j = 0;
        var item = jQuery(falseSelect).find('.select-item').clone();
        var activeSelectValue = falseSelect.find('select').find(":selected").text();
        var activeSelectChild = 1;
        var HTMLstring = "";
        for (j; j < optionNumber; j++) {

            if (activeSelectValue == optionName[j]) {
                activeSelectChild = j + 1;
            }
            jQuery(item[0]).text(optionName[j]);
            HTMLstring += item[0].outerHTML;
        }

        jQuery(falseSelect).find('.select-items').html(HTMLstring);
        // add selected value on initialisation
        jQuery(falseSelect).find('.select-items .select-item:nth-child(' + activeSelectChild + ')').addClass('active');

        /**
         * when false select is clicked
         */
        falseSelect.on('click', function () {

            /* prevent multiple click */
            if ($(this).find('.select-items').is(':animated')){
                return false;
            }

            /* close item when click again */
            if ($(this).find('.select-title').hasClass('active')) {
                $(this).find('.select-items').slideToggle();
                $(this).find('.select-title').removeClass('active');

                return false;
            }

            /* open slide */
            if (!$(this).find('.select-title').hasClass('active')) {

                $(this).find('.select-title').addClass('active');
                $(this).find('.select-items').slideToggle();

            }

        });

        /**
         * click on false select option
         */
        falseSelect.find('.select-item').on('click', function () {

            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var itemSelected = $(this).index();
            var allOptions = $(this).closest('.elexyr-select').find('select option');

            /* remove default option value */
            if ($(allOptions[0]).attr('disabled')){
                allOptions = allOptions.slice(1);
                optionNumber -= 1;
            }

            /* remove selected if option is already selected */
            if (allOptions[itemSelected].hasAttribute('selected')){
                $(allOptions[itemSelected]).removeAttr("selected","selected");
                falseSelect.find('select').val([]);
                $(this).removeClass('active');
                $(this).closest('.select-items').slideToggle();
                return false;
            }

            /* add selected to the right element */
            $(allOptions).removeAttr("selected","selected");
            falseSelect.find('select').val([]);
            $(allOptions[itemSelected]).attr("selected","selected");
        });

    },

    triggerMediaInput: function (mediaImageClass, inputId) {
        var mediaImageSelector = jQuery(mediaImageClass);

        mediaImageSelector.on('click', function () {
            mediaImageSelector.removeClass('active');
            jQuery(this).addClass('active');
            // pre fill input with selected media
            var mediaId = jQuery(this).data('value-id');
            jQuery(inputId).val(mediaId);
        });
    },

    /**
     * color preview for input fields
     * @param colorPreviewContainer
     */
    colorPreview: function (colorPreviewContainer) {
        var container = jQuery(colorPreviewContainer);

        container.find('input').on('keyup', function () {
            var inputVal = jQuery(this).val();
            jQuery(this).siblings('div').css('background-color', inputVal);
        })
    }
};