var Page = {

    /**
     * clone the div
     * @param button
     * @param elementToDuplicate
     * @param closestElement
     * @param elementToDuplicateGroup
     */
    cloneSection: function (button, elementToDuplicate, closestElement, elementToDuplicateGroup) {

        button = jQuery(button);

        button.on('click',function () {
            var buttonText = $(this).text(), i;
            var copie = $(this).closest(closestElement).find(elementToDuplicate).clone();
            var elementsToReplace = $(this).closest(closestElement).find(elementToDuplicateGroup);

            var HTMLstring="";

            for (i=1; i <= buttonText; i++){
                HTMLstring += copie[0].outerHTML;
            }

            elementsToReplace.html(HTMLstring);
        })

    },

    /**
     * display the container to select the number of items
     * @param buttonToActive
     * @param section
     * @param classToAdd
     */
    displayCloneSection: function (buttonToActive, section, classToAdd) {

        buttonToActive = jQuery(buttonToActive);

        buttonToActive.on('click',function () {

            if (($(this).closest('.grid').find(section)).is(':visible')){
                $(this).closest('.grid').find(section).removeClass(classToAdd);
                $(this).closest('.grid').find(section).siblings('.builder-section-items').attr('style','display:flex !important');

                return false;
            }

            $(this).closest('.grid').find(section).siblings('.builder-section-items').attr('style','display:none !important');
            $(this).closest('.grid').find(section).addClass(classToAdd);

            $('.builder-section-number .button-default').on('click',function () {
                $(this).closest('.grid').find(section).removeClass(classToAdd);
                $(this).closest('.grid').find(section).siblings('.builder-section-items').attr('style','display:flex !important');
            })

        })

    }

};