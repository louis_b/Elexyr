var Tools = {

    /**
     * the drag and drop function
     * @param itemsToDrop
     * @param dataName
     * @param textContainer
     * @param ajaxOn
     */
    dragAndDrop: function(itemsToDrop, dataName, textContainer, ajaxOn){

    // on ajoute dynamiquement la position des éléments
    $(itemsToDrop).each(function(e){
      var index = e;
      $(this).attr(dataName, index.toString());
    });

    $(itemsToDrop).on({

      // on commence le drag
      dragstart: function(e) {
          $this = $(this);

          i = $this.index();

          // on garde le texte en mémoire (A, B, C ou D)
          e.originalEvent.dataTransfer.setData('text', $this.find(textContainer).text());
          e.originalEvent.dataTransfer.setData('data-id', $this.attr('data-id'));
      },
      // on passe sur un élément draggable
      dragenter: function(e) {

          e.preventDefault();
      },
      // on quitte un élément draggable
      dragleave: function() {

      },
      // déclenché tant qu on a pas lâché l élément
      dragover: function(e) {
          e.preventDefault();
      },
      // on lâche l élément
      drop: function(e) {
          // si l élément sur lequel on drop n'est pas l'élément de départ
          if (i !== $(this).index()) {
              // on récupère le texte initial
              var data = e.originalEvent.dataTransfer.getData('text');
              var dataId = e.originalEvent.dataTransfer.getData('data-id');

              // on met le nouveau texte à la place de l ancien et inversement
              var liDrop = $(this);
              var liOld = $this;
              liOld.find(textContainer).text(liDrop.find(textContainer).text());
              liDrop.find(textContainer).text(data);

              if ( typeof ajaxOn !== 'undefined' && typeof ajaxOn !== false ){
                  // on intervertit les ID
                  var OldId = liDrop.attr('data-id');
                  var liOldId = liOld.attr('data-id', OldId).attr('data-id');
                  var liDropId = liDrop.attr('data-id', dataId).attr('data-id');

                  // on intervertit les positions
                  var liDropPosition = liDrop.data('position');
                  var liOldPosition = liOld.data('position');

                  // on intervertit les liens de suppression
                  var deleteLink = Tools.sliceLastValue(liOld.find('a').attr('data-href'));
                  liOld.find('a').attr('data-href', deleteLink + liOldId);
                  liDrop.find('a').attr('data-href', deleteLink + liDropId);

                  // enregistrement en Ajax du drag and drop
                  Tools.ajaxUpdatePosition(liDropId, liDropPosition, liOldId, liOldPosition);

                  var builderGroupDrop = $("#builder").find('.builder-section-group[data-id="'+ liDropId +'"]').find('.builder-section-items');
                  var builderGroupOld = $("#builder").find('.builder-section-group[data-id="'+ OldId +'"]').find('.builder-section-items');

                  builderGroupOld.closest('.builder-section-group').attr('data-id', liDropId);
                  builderGroupDrop.closest('.builder-section-group').attr('data-id', liOldId);

                  Tools.swapElement(builderGroupDrop, builderGroupOld);

              }
          }

      },
      // fin du drag (même sans drop)
      dragend: function() {

      },
      // au clic sur un élément
      click: function(e) {
          e.preventDefault();
      }

    })

    },

    /**
     * ajax method to update dynamically section position
     * @param newId
     * @param newValue
     * @param oldId
     * @param oldValue
     */
    ajaxUpdatePosition: function(newId, newValue, oldId, oldValue){

          // bad method to get current baseUrl
          var getUrl = window.location;
          var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

          $.ajax({
              type: 'POST',
              url: baseUrl + '/pages/section/update',
              timeout: 3000,
              data: {
                  old_value: oldValue,
                  old_id: oldId,
                  new_value: newValue,
                  new_id: newId
              },
              success: function(data) {
              },
              error: function() {
                  alert('La requête n\'a pas abouti');
              }
          });
      },

    /**
     * ajax method to insert dynamically the section in bdd
     * @param button
     */
    ajaxInsertSection: function (button) {

        button = $(button);
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        var pageId = Tools.getUrlLastParam(getUrl.href);

        button.on('click', function () {
            var $this = $(this);
            var buttonValue = $(this).text();
            var sectionId = $(this).closest('.builder-section-group').attr('data-id');

            $.ajax({
                type: 'POST',
                url: baseUrl + '/pages/content/create',
                timeout: 3000,
                dataType: "json",
                data: {
                    content_number: buttonValue,
                    page_id: pageId,
                    section_id: sectionId
                },
                success: function(data) {
                    var idToSend = $this.closest('.builder-section-group').find('.builder-section-item');
                    var oldDataId = $this.closest('.builder-section-group').find('.builder-section-item').first().attr('data-id');

                    for(var i = 0; i < data.length; i++) {
                        var currentUrl = jQuery(idToSend[i]).find('a').attr('href');
                        var obj = data[i];
                        currentUrl = Tools.replaceWith(currentUrl, oldDataId.toString(), obj.entity_id);
                        jQuery(idToSend[i]).find('a').attr('href', currentUrl);
                        jQuery(idToSend[i]).attr('data-id', obj.entity_id);
                    }
                },
                error: function() {
                    alert('La requête n\'a pas abouti');
                }
            });
        })

    },

    /**
     * return a string without the value after the last "/"
     * @param string
     * @returns string
     */
    sliceLastValue: function(string) {
        return string.substring(0, string.lastIndexOf("/") + 1);
    },

    /**
     * replace old value by another one
     * @param url
     * @param oldValue
     * @param newValue
     * @returns {string}
     */
    replaceWith: function(url, oldValue, newValue) {

        url = url.replace(oldValue,newValue);

        return url;
    },

    /**
     * return URL last param
     * @param url
     * @returns {string}
     */
    getUrlLastParam: function (url) {
        return url.substring(url.lastIndexOf('/') + 1);
    },

    /**
     * return site url
     * @returns {string}
     */
    getSiteUrl: function () {
        var getUrl = window.location;
        return getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    },

    swapElement: function (firstElement, secondElement) {
        var firstElementClone = firstElement.clone();
        var secondElementClone = secondElement.clone();

        firstElement.replaceWith(secondElementClone);
        secondElement.replaceWith(firstElementClone);
    },

    closeFormError: function (button, divToClose) {

        button = jQuery(button);
        divToClose = jQuery(divToClose);

        button.on('click', function () {
            divToClose.fadeOut('slow');
        });

    }

};
