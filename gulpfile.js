// load packages
var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var useref = require('gulp-useref');
var cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var uncss = require('gulp-uncss');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
const imagemin = require('gulp-imagemin');
var replace = require('gulp-url-replace');
const fs = require('fs');
const path = require('path');
const DS = path.sep;
var rename = require("gulp-rename");

const rootDir = fs.realpathSync("./");
const srcDir = rootDir + DS + "src";
const pluginsDir = rootDir + DS + "plugins";
const webDir = rootDir + DS + "web";

// paths
var paths = {
    // dev
    devScss: srcDir + '/**/*.scss',
    pluginsScss: pluginsDir + '/**/*.scss',
    devCss: webDir + '/bundle/',
    devAllFiles: srcDir + "/**/*",
    pluginsAllFiles: pluginsDir + "/**/*",
    devAllJs: '*.js',
    devAllCss: '*.css',
    devImgDir: srcDir + '/**/img/**/*',
    devFontDir: srcDir + '/**/fonts/**/*',
    pluginFontDir: pluginsDir + '/**/fonts/**/*',
    devJsDir: srcDir + '/**/js/**/*',

    //prod
    prodHtmlDir: 'app/*.php',
    prodDir: webDir
};

// sass compilation
gulp.task('sass', function(){
    gulp.src(paths.devScss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        // cleanCSS with no url rebasing
        .pipe(new cleanCSS({
            rebaseTo: false,
            rebase: false
        }))
        /*        .pipe(uncss({
                    html: [paths.prodHtmlDir]
                }))*/
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace(/View\/assets\/scss/i,'css');
            path.extname = path.extname.replace(/.css/,'.min.css')
        }))
        .pipe(gulp.dest(paths.devCss));
});

gulp.task('sass-p', function(){
    gulp.src(paths.pluginsScss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        // cleanCSS with no url rebasing
        .pipe(new cleanCSS({
            rebaseTo: false,
            rebase: false
        }))

        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace(/View\/assets\/scss/i,'css');
            path.extname = path.extname.replace(/.css/,'.min.css')
        }))

        .pipe(gulp.dest(paths.devCss));
});

// js compilation
gulp.task('js', function() {
    return gulp.src(paths.devJsDir)
/*
        .pipe(uglify())
*/
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace(/View\/assets\/js/i,'js');
        }))
        .pipe(gulp.dest(paths.devCss))

        .pipe(concat('main.min.js'))

});

// watcher
gulp.task('watch', function(){
    gulp.watch(paths.devAllFiles, ['sass','js']);
});

// plugins watcher
gulp.task('watch-p', function(){
    gulp.watch(paths.pluginsAllFiles, ['sass-p']);
});

// prod-minify
gulp.task('prod-minify', function () {
    return gulp.src(paths.prodHtmlDir)
        .pipe(useref())
        .pipe(gulpif(paths.devAllCss, cleanCSS()))
        .pipe(gulpif(paths.devAllJs, uglify()))
        .pipe(gulp.dest(paths.prodDir))
});

// copy fonts to prod
gulp.task('copyfonts', function() {
    gulp.src(paths.devFontDir)
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace(/View\/assets\/fonts/i,'fonts');
        }))
        .pipe(gulp.dest(paths.devCss));
});

// copy plugins-fonts to prod
gulp.task('copyfonts-p', function() {
    gulp.src(paths.pluginFontDir)
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace(/View\/assets\/fonts/i,'fonts');
        }))
        .pipe(gulp.dest(paths.devCss));
});

// copy img to prod
gulp.task('copyimg', function() {
    gulp.src(paths.devImgDir)
        .pipe(imagemin([
            imagemin.optipng({optimizationLevel: 3})
        ]))
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace(/View\/assets\/img/i,'img');
        }))
        .pipe(gulp.dest(paths.devCss));
});

// unCss
gulp.task('uncss', function () {
    return gulp.src([paths.prodDir + '/assets/css/**/*.css', '!dist/assets/css/prism.min.css'])
        .pipe(uncss({
            html: ['dist/*.php'],
            ignore: [/\.z-fixed/]
        }))
        .pipe(gulp.dest(paths.prodDir + '/assets/css/'));
});

// run this task to create prod environment
gulp.task('prod', function(done) {
    runSequence('sass','copyimg','js', function() {
            console.log('have fun :)');
    });
});
