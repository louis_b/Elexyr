<?php

use app\Core\Onyx;

// run Onyx framework
require __DIR__ . '/app/framework/onyx/app/Core/Onyx.php';
require __DIR__ . '/lib/phpmailer/PHPMailerAutoload.php';
Onyx::run();